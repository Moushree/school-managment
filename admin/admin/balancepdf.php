<?php
include('mpdf/vendor/autoload.php');

include('inc/db.php');



$html='<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Tethibari Sarada Sishu Vidyamandir</title>
    <link rel="stylesheet" href="balancecss/style.css" media="all" />
  </head>
  <body>
    <header class="">
      <div style="text-align:center;">
        <img src="image/tssv-logo.png" style="width:100px; height:100px;" >
      </div>
    
      <div style="text-align:center; padding-bottom:20px;">
        <div>Tethibari Sarada Sishu Vidyamandir</div>
        <div>Estd. - 1998</div>
        <div>Regd. No - 48426 of 2007-08</div>
        <div>Tethibari || Kismat Bajkul || Purba Medinipur</div>
        
      </div>
      <h1>Balance Sheet</h1>

    </header>
    <main>
     <h2 style="text-align:center">Credit Details</h2>
      <table>
       
        <thead>
         
          <tr>
            <th style="text-align:left; font-size:14px; font-weight:bold;">Pay to/ Received from</th>
            <th class="desc" style="text-align:left; font-size:14px; font-weight:bold;">Purpose</th>
            <th style="text-align:left; font-size:14px; font-weight:bold;">Amount</th>
            <th style="text-align:left; font-size:14px; font-weight:bold;">Date</th>
          </tr>
        </thead>
        <tbody>';
    
            $month=$_POST['month'];
            $year=$_POST['year'];
            $flag=0;
            $sel="SELECT * FROM account WHERE ( MONTHNAME(paydate)='$month' AND YEAR(paydate)=$year AND status=1) ORDER BY id DESC";
            $rs=$con->query($sel);
            
                while($row=$rs->fetch_assoc())
                {
                  $flag=1;
                  $html.= '<tr>';
                if(is_numeric($row['p_name'])){
                    $sid=$row['p_name'];
                    $sels="SELECT studentinfo.*, addclass.* FROM studentinfo LEFT JOIN addclass ON studentinfo.class=addclass.id WHERE studentinfo.id=$sid";
                    $res=$con->query($sels);
                    while($rows=$res->fetch_assoc()){
                       $html.='<td  style="text-align:left;">'.$rows['sname'].'('.$rows['cname'].','.$rows['section'].','.$rows['roll'].')</td>';
                    }
                   $html.='<td  class="desc">Fees For:'.$row['purpose'].'</td>';
                  }
                  else{
                     $html.='<td  style="text-align:left;">'.$row['p_name'].'</td>
                        <td  class="desc">'.$row['purpose'].'</td>';
                  }
                  
                   $html.='<td  style="text-align:left;">'.$row['amount'].'</td>';
                   $py_dt= new DateTime($row['paydate']);
                   
                  $html.= '<td  style="text-align:left;">'. $py_dt->format('d-m-Y').'</td>
                   </tr>';
                   
                }

               if($flag==0){
                $html.= '<tr>
                   <td  style="text-align:center;" colspan="4">No Data Available</td>
                  </tr>';
            }
        $html.='</tbody>
      </table>
      <h2 style="text-align:center; padding-top:20px;">Debit Details</h2>
      <table>
       
        <thead>
         
          <tr>
            <th style="text-align:left;">Pay to/ Receive from</th>
            <th class="desc">Perpose</th>
            <th style="text-align:left;">Amount</th>
            <th style="text-align:left;">Date</th>
          </tr>
        </thead>
        <tbody>';
    
            $month=$_POST['month'];
            $year=$_POST['year'];
            $sel="SELECT * FROM account WHERE ( MONTHNAME(paydate)='$month' AND YEAR(paydate)=$year AND status=0) ORDER BY id DESC";
            $rs=$con->query($sel);
            if($rs->num_rows>0){
                while($row=$rs->fetch_assoc())
                {
                  $html.= '<tr>
                   <td  style="text-align:left;">'.$row['p_name'].'</td>
                   <td  class="desc">'.$row['purpose'].'</td>
                   <td  style="text-align:left;">'.$row['amount'].'</td>';
                   $py_dt= new DateTime($row['paydate']);
                   
                  $html.= '<td  style="text-align:left;">'. $py_dt->format('d-m-Y').'</td>
                   </tr>';
                }
            }
            else{
                $html.= '<tr>
                   <td  style="text-align:center;" colspan="4">No Data Available</td>
                  </tr>';
            }
        $html.='</tbody>
      </table>
      

      
    </main>

  </body>
</html>';
$mpdf=new \Mpdf\Mpdf();

$mpdf->AddPage('','E');
    
$mpdf->AddPage();

$mpdf->SetHTMLFooter('
    <footer style="text-align:center; ">
      Tethibari Sarada Sishu Vidyamandir || https://tssv.in
    </footer>
    ','O');
$mpdf->SetHTMLFooter('
    <footer style="text-align:center; ">
      Tethibari Sarada Sishu Vidyamandir || https://tssv.in
    </footer>
    ','E');

$css=file_get_contents('balancecss/style.css');
$mpdf->WriteHTML($css,1);
$mpdf->WriteHTML($html);

$mpdf->Output('balance_sheet_'.date("M-d-Y_H_i_s").'.pdf','D');
exit;
?>