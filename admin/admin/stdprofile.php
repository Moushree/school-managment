<?php

include("inc/db.php");
session_start();
if(!isset($_SESSION['uid']))
{
    header("location:index.php");
}
?>
<!DOCTYPE html>
<html>
  <?php include("inc/header.php");?>
 


<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  <?php include("inc/navbar.php");
        include("inc/sidebar.php")
  ?>

 <style>
     img.profileimg{
    width: 145px;
    align-items: center;
    margin-left: 88px;
    }
 </style> 

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
       <!--     <h1 class="m-0 text-dark">Student Details Form</h1>-->
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">

            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Student Details Form</h3>

             </div>
            <form id="stdForm" >
                <div class="card-body">
                 <div class="row">
                     <div class="col-md-8">
                         <div class="form-group" id="namediv">
                            <label>Enter Student Name:<span class="text-danger">*</span></label>
                            <input type="text" class="form-control"  id="stdname" />
                           
                          </div>
                          <div class="form-group" id="gnamediv">
                            <label>Enter Guardian Name:<span class="text-danger">*</span></label>
                            <input type="text" class="form-control"  id="grdname" />
                           
                          </div>
                          <div class="row">
                            <div class="col-md-6" id="gendiv">
                              <div class="form-group" id="gender">
                     
                				<label>Select Gender: <span class="text-danger">*</span></label><br>
                				<label for="gender_male">
                					<input type="radio" class="gender" id="gen1" name="gender" value="Male" > Male
                				</label><br>
                				<label for="gender_female">
                					<input type="radio" class="gender" id="gen2" name="gender"  value="Female"> Female
                				</label><br>
                                </div>
                            </div>
                            <div class="col-md-6" id="datediv">
                                 <!-- Date -->
                                <div class="form-group">
                                  <label>Date Of Birth(DD/MM/YYYY):<span class="text-danger">*</span></label>
                                    <div class="input-group date" id="reservationdate" data-target-input="nearest">
                                        <input type="text" class="form-control datetimepicker-input" data-target="#reservationdate" id="stddate"/>
                                        <div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
                                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                        </div>
                                    </div>
                                </div>
                           </div> 
                        </div>
                     </div>
                     <div class="col-md-4">
                        <img src="image/profile-male.png" class="profileimg" id="profileimg" style="border:1px solid #ccc; padding:10px; width:150px;">
                        <input type="file" id="img" name="img" style="width: 11.5rem; margin-left: 71px; margin-top: 10px;"  onchange="validateImage()"><span id="imgRemove" style="margin-left: 20px;" data-toggle="tooltip" title="Remove Image"  style="cursor:pointer;"><i class="fas fa-times"></i></span><br>
                        <p class="text-center"><strong id="imgerror"></strong></p>
                     </div>
                 </div>

                 <div class="form-group" id="adddiv">
                    <label>Enter Address:<span class="text-danger">*</span></label>
                    <textarea class="form-control" id="stdadd"></textarea>
                    <span id="errmsg3"></span>
                  </div>
                  <div class="form-group">
                    
                    <div class="row">
                        <div class="col-md-6" id="podiv">
                           <label>Enter Post Office:<span class="text-danger">*</span></label>
                           <input type="text" class="form-control" id="stdpo"/> 
                        </div>
                        <div class="col-md-6" id="vlgdiv">
                            <label>Enter Village:<span class="text-danger">*</span></label>
                            <input type="text" class="form-control" id="stdvlg" >
                        </div>
                    </div>
                 
                  </div>
                  <div class="form-group">
                    
                    <div class="row">
                        <div class="col-md-6" id="psdiv">
                           <label>Enter Police Station:<span class="text-danger">*</span></label>
                           <input type="text" class="form-control" id="stdps"/> 
                        </div>
                        <div class="col-md-6" id="distdiv">
                            <label>Enter District:<span class="text-danger">*</span></label>
                            <input type="text" class="form-control" id="stddist" >
                        </div>
                    </div>
                 
                  </div>
                  <div class="form-group">
                    
                    <div class="row">
                        <div class="col-md-6" id="pindiv">
                           <label>Enter Pincode:<span class="text-danger">*</span></label>
                           <input type="text" class="form-control" id="stdpin"/> 
                        </div>
                        <div class="col-md-6">
                            <label>Enter AADHAR NO:</label>
                            <input type="text" class="form-control" id="stdaadhar" >
                        </div>
                    </div>
                 
                  </div>
                  <div class="form-group">
                    
                    <div class="row">
                        <div class="col-md-6" id="phdiv">
                           <label>Enter Mobile Number:<span class="text-danger">*</span></label>
                           <input type="text" class="form-control" id="stdnum1"/> 
                        </div>
                        <div class="col-md-6">
                            <label>Enter Mobile Number(Optional):</label>
                            <input type="text" class="form-control" id="stdnum2" >
                        </div>
                    </div>
                 
                  </div>
                  <div class="form-group" id="emaildiv">
                    <label>Enter Email(optional):</label>
                    <input type="text" class="form-control" id="stdemail"/>
                 </div>
                  <div class="row">
                      <div class="col-md-4">
                          <div class="form-group" id="classdiv">
                            <label>Enter Class:<span class="text-danger">*</span></label>
                                <select class="form-control" id="stdclass">
                                    <option></option>
                                    <?php
                                    $sel="SELECT * FROM addclass";
                                    $rs=$con->query($sel);
                                  
                                        while($row=$rs->fetch_assoc())
                                        {
                                        ?>
                                        <option value="<?php echo $row['id'];?>"><?php echo $row['cname'];?></option>
                                        <?php
                                        }
                                    
                                    ?>
                                </select>
                            
                          </div>
                      </div>
                      <div class="col-md-4">
                          <div class="form-group" id="secdiv">
                            <label>Enter Section:</label>
                                 <input type="text" id="stdsection"  class="form-control"/>
                            
                          </div>
                      </div>
                  </div>
                 <div class="row">
                      <div class="col-md-4">
                          <div class="form-group" id="rolldiv">
                            <label>Enter Roll Number:</label>
                            <input type="text" class="form-control" id="stdroll" />
                           
                          </div>
                      </div>
                      <div class="col-md-4">
                          <div class="form-group" id="bgdiv">
                            <label>Enter Blood Group:</label>
                            <input type="text" class="form-control" id="stdgroup" />
                            
                          </div>
                      </div>
                  </div>

                </div>
                <p id="stderrmsg"></p>
                <!-- /.card-body -->
                <div class="card-footer">
                <input type="submit" class="btn btn-primary" id="sub" value="Add Student">
                <input type="submit" class="btn btn-primary" value="Add Student And Send Massage">
                </div>
              </form>
        
      
      </div>
    <div class="alert alert-default" style="display:none;" id="alertmsg">
        <span class="msgclose float-right" id="msgid" style="cursor:pointer;"><i class="fas fa-times"></i></span>
        <strong id="msg"></strong>
    </div>
    </section>
    <!-- /.content -->
  </div>
  
<!-------------style for form validation---------------------->

<style>

.error_form {
    border: 2px solid red;
}
.error {
  color: red;
  margin-left: 5px;
}

label.error {
  display: inline;
  
}
</style>


<?php include("inc/footer.php");?>


<script>

    $('#reservationdate').datetimepicker({
        format: 'DD/MM/YYYY'
    });
	
/*----------------------------gender checking-------------------------	*/

$(document).on('change','.gender',function(){
    console.log(document.getElementById('img').files.length);
    if(document.getElementById('img').files.length<=0)
    {
        
        var gender;
        if($(this).is(':checked'))
        {
            gender=$(this).val();
            
        }
        console.log(gender);
        if(gender =='Male'){
            $('#profileimg').attr('src','../admin/image/profile-male.png');
        }
        else
        {
            $('#profileimg').attr('src','../admin/image/profile-female.png');
        }
    }
});



/*----------------------------image validation-------------------------	*/
	function validateImage(){
	   
	   var check_img= false;
	   var imgfile= document.getElementById('img').files[0]; 
	   console.log(imgfile);
	   
	   var _URL = window.URL || window.webkitURL;
            
       var file, img, height, width;
        if ((file = imgfile)) {
            img = new Image();
            var objectUrl = _URL.createObjectURL(file);
            img.onload = function () {
                width=this.width;
                height=this.height;
                if ((height > 600 && width > 600) || (height > 1200 && width > 1200)) {
                  $('#imgerror').html(" Upload  min 600*600 & max 1200*1200 photo size");
                  $('#imgerror').css('color','red');
                  document.getElementById("img").value = '';
                }
                else{
                $('#imgerror').html(" ");
                $('#profileimg').attr('src', objectUrl);
                check_img = true;
                return true;  
                }
               
                _URL.revokeObjectURL(objectUrl);
            };
            img.src = objectUrl;
        }
       
	  
	   var formdata=new FormData();
	   
	           
        var t = imgfile.type.split('/').pop().toLowerCase();
        var width = imgfile.width;
        var height = imgfile.height;
        console.log(height);
        if (t != "jpeg" && t != "jpg" && t != "png" && t != "JPEG" && t != "JPG") {
            $('#imgerror').html("Please select a valid image file");
            $('#imgerror').css('color','red');
            document.getElementById("img").value = '';
            return false;
        }
        else if (imgfile.size >=400000) {
            $('#imgerror').html("Max Upload size is 400kb only");
            $('#imgerror').css('color','red');
            document.getElementById("img").value = '';
            return false;
        }
        else{
        $('#imgerror').html(" ");
        check_img = true;
        return true;  
        };
        
        if(check_img == true){
            $('#profileimg').attr('src',objectUrl)
        }
	};
/*------------------ remove profile image--------------------------*/	

$(document).on('click','#imgRemove',function(){

   if($("#gen1").prop("checked"))
   {
     $('#profileimg').attr('src','../admin/image/profile-male.png');   
   }
   else
   {
     $('#profileimg').attr('src','../admin/image/profile-female.png');  
   }
   
   $('#img').val("");
    
});



// submit form


  $('#stdForm').on('submit',function(e){
      
      e.preventDefault();
       
        var sn=$('#stdname').val();
        var gn=$('#grdname').val();
        var g=$('.gender:checked').val();
        var d=$('#stddate').val();
        var add=$('#stdadd').val();
        var po=$('#stdpo').val();
        var vlg=$('#stdvlg').val();
        var ps=$('#stdps').val();
        var dist=$('#stddist').val();
        var pin=$('#stdpin').val();
        var aadhar=$('#stdaadhar').val();
        var ph1=$('#stdnum1').val();
        var ph2=$('#stdnum2').val();
        var e=$('#stdemail').val();
        var c=$('#stdclass').val();
        var sec=$('#stdsection').val();
        var roll=$('#stdroll').val();
        var bg=$('#stdgroup').val();
        var imgfile= document.getElementById('img').files[0];
        
        
        
        // form validation
            
        $(".error").remove();
        $("#stdForm").removeClass('error_form')
    
        if (sn.length < 1) {
          $('#stdname').after('<span class="error">This field is required</span>');
          $('#stdForm').addClass('error_form');
          $('html, body').animate({
                scrollTop: $("#namediv").offset().top
            }, 500);
          return false;
        }
        if (gn.length < 1) {
          $('#grdname').after('<span class="error">This field is required</span>');
          $('#stdForm').addClass('error_form');
          $('html, body').animate({
                scrollTop: $("#gnamediv").offset().top
            }, 500);
          return false;
        }
        if ($('input[type=radio][name=gender]:checked').length == 0) {
          $('#gender').after('<span class="error">This field is required</span>');
          $('#stdForm').addClass('error_form');
          $('html, body').animate({
                scrollTop: $("#gendiv").offset().top
            }, 500);
          return false;
        }
        
        if (d.length < 1) {
          $('#reservationdate').after('<span class="error">This field is required</span>');
          $('#stdForm').addClass('error_form');
          $('html, body').animate({
                scrollTop: $("#datediv").offset().top
            }, 500);
          return false;
        }
         if (add.length < 1) {
          $('#stdadd').after('<span class="error">This field is required</span>');
          $('#stdForm').addClass('error_form');
          $('html, body').animate({
                scrollTop: $("#adddiv").offset().top
            }, 500);
          return false;
        }
        if (po.length < 1) {
          $('#stdpo').after('<span class="error">This field is required</span>');
          $('#stdForm').addClass('error_form');
          $('html, body').animate({
                scrollTop: $("#podiv").offset().top
            }, 500);
          return false;
        }
         if (vlg.length < 1) {
          $('#stdvlg').after('<span class="error">This field is required</span>');
          $('#stdForm').addClass('error_form');
          $('html, body').animate({
                scrollTop: $("#vlgdiv").offset().top
            }, 500);
          return false;
        }
         if (ps.length < 1) {
          $('#stdps').after('<span class="error">This field is required</span>');
          $('#stdForm').addClass('error_form');
          $('html, body').animate({
                scrollTop: $("#psdiv").offset().top
            }, 500);
          return false;
        }
         if (dist.length < 1) {
          $('#stddist').after('<span class="error">This field is required</span>');
          $('#stdForm').addClass('error_form');
          $('html, body').animate({
                scrollTop: $("#distdiv").offset().top
            }, 500);
          return false;
        }
         if (pin.length < 1) {
          $('#stdpin').after('<span class="error">This field is required</span>');
          $('#stdForm').addClass('error_form');
          $('html, body').animate({
                scrollTop: $("#pindiv").offset().top
            }, 500);
          return false;
        }
      
        if (ph1.length < 1) {
          $('#stdnum1').after('<span class="error">This field is required</span>');
          $('#stdForm').addClass('error_form');
          $('html, body').animate({
                scrollTop: $("#phdiv").offset().top
            }, 500);
          return false;
        }
        if(isNaN(ph1)){
            $('#stdnum1').after('<span class="error">Enter a valid phone number</span>');
            $('#stdForm').addClass('error_form');
            return false;
        }
      
        if (e.length > 1) {
          var regEx =/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
          var validEmail = regEx.test(e);
          if (!validEmail) {
            $('#stdemail').after('<span class="error">Enter a valid email</span>');
            $('#stdForm').addClass('error_form');
            $('html, body').animate({
                scrollTop: $("#emaildiv").offset().top
            }, 500);
            return false;
          }
        }
      if (c.length < 1) {
          $('#stdclass').after('<span class="error">Select Any One Option</span>');
          $('#stdForm').addClass('error_form');
          $('html, body').animate({
                scrollTop: $("#classdiv").offset().top
            }, 500);
          return false;
        }

        var formdata=new FormData();
        formdata.append('stdname',sn);
        formdata.append('grdname', gn);
        formdata.append('gender',g);
        formdata.append('stddate',d);
        formdata.append('stdadd',add);
        formdata.append('stdpo',po);
        formdata.append('stdvlg',vlg);
        formdata.append('stdps',ps);
        formdata.append('stddist',dist);
        formdata.append('stdpin',pin);
        formdata.append('stdaadhar',aadhar);
        formdata.append('stdnum1',ph1);
        formdata.append('stdnum2',ph2);
        formdata.append('stdemail',e);
        formdata.append('stdclass',c);
        formdata.append('stdsection',sec);
        formdata.append('stdroll',roll);
        formdata.append('stdgroup',bg);
        formdata.append('img',imgfile);
    
         $.ajax({
            url:"studentins.php",
            type:"post",
            data:formdata,
            processData:false,
            contentType:false,
            dataType:'json',
            success:function(data)
            {
             
              console.log(data);

               
            },
            complete:function(data)
            {
                console.log(data);
                if(data.responseJSON.msg == "true")
                {
                    $('#alertmsg').slideDown();
                    $('#msg').html("Data Inserted Successfully");
                    $('#msg').css('color','green');
                    $('#stdForm')[0].reset();
                    $('#profileimg').attr("src","../admin/image/profile-male.png");
                }
                else if(data.responseJSON.msg == "stderr")
                {
                   $('#alertmsg').slideDown();
                   $('#msg').html("Student Already Exist Of Roll:"+" "+data.responseJSON.roll+" in Sction:"+data.responseJSON.sec);
                   $('#msg').css('color','red'); 
                }
                else
                {
                   $('#alertmsg').slideDown();
                   $('#msg').html("Some Error Occure");
                   $('#msg').css('color','red');
                }
   
            }
            
        });   

      });
     $(document).on('click','#msgid',function(){
        $("#alertmsg").slideUp();
    });

    
    
</script>
</body>
</html>
