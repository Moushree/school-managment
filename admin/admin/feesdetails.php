<?php

include("inc/db.php");
session_start();
if(!isset($_SESSION['uid']))
{
    header("location:index.php");
}
?>
<!DOCTYPE html>
<html>
<?php include("inc/header.php");?>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  <?php include("inc/navbar.php");
        include("inc/sidebar.php")
  ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">



    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
    <div class="alert alert-default" style="display:none;" id="alertmsg">
        <span class="msgclose float-right" id="msgid" style="cursor:pointer;"><i class="fas fa-times"></i></span>
        <strong id="msg"></strong>
    </div>
        <div class="row">
          <div class="col-12">

            <?php
                    $cid=$_GET['id']; // class id
                    $selc="SELECT * FROM addclass WHERE id='$cid'";
                    $rsc=$con->query($selc);
                    while($rowc=$rsc->fetch_assoc())
                    {
                        $classid=$rowc['id'];
                        $classname = $rowc['cname'];
                    }
                    
                    ?>

            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Payment History Of : <?php echo $classname; ?></h3>
                <a href="viewstudent.php?id=<?php echo $classid;?>" class="btn btn-primary float-right text-white">Back <i class="fas fa-arrow-circle-right"></i></a>
              </div>
              <!-- /.card-header -->
              <div class="card-body table-responsive">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>Name</th>
                    <th>Roll</th>
                    <th>Section</th>
                    <th>Amount</th>
                    <th>Month</th>
                    <th>Date</th>
                    <th>View</th>
                    <th>Edit</th>
                    <th>Delete</th>
                  </tr>
                  </thead>
                  <tbody id="fees-tbody">
                      
                    <?php
                    $id=$_GET['id']; // class id
                    $sel="SELECT studentinfo.*, fees_table.* FROM studentinfo RIGHT JOIN fees_table ON studentinfo.id = fees_table.sid WHERE class=$id ORDER BY fees_table.feedate";
                    $rs=$con->query($sel);
                    while($row=$rs->fetch_assoc())
                    {
                          
                    ?>
                           <tr>
        
                            <td class="std-name"><?php echo $row['sname'];?></td>
                            <td class="std-roll"><?php echo $row['roll'];?></td>
                            <td class="std-sec"><?php echo $row['section'];?></td>
                            <td class="std-amt"><?php echo $row['total'];?></td>
                            <td class="std-month"><?php echo $row['month'];?></td>
                            <?php
                             $dt = new DateTime($row['feedate']);
                            ?>
                            <td><?php echo $dt->format('d/m/Y');?></td>
                            <td><a href="viewstudentsingle.php?id=<?php echo $row['sid'] ?>" class="text-success text-center"><i class="far fa-eye" aria-hidden="true"></i></a></td>
                            <td><a href="editfees_payment.php?id=<?php echo $row['id'] ?>"  id="editbtn" class="text-center text-primary"><span class="editid d-none"><?php echo $row['id'];?></span><i class="fas fa-edit" aria-hidden="true"></i></a></td>
                            <td><a  type="button" onclick="deldata(<?php echo $row['id'];?>)" id="delbtn" class="text-center text-danger"><i class="far fa-trash-alt" aria-hidden="true"></i></a></td>
                           </tr>
                    <?php
                        
                    }
                    ?>

                  </tbody>   
                
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<!-- -------------------Edit modal ------------------->
 
 <div class="modal fade" id="editModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Edit Fees Payment</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          
        </div>
        <div class="modal-body">
  
            <div class="card-body">
              <input type="hidden"  class="form-control classfield" id="modaleditid">
                 <div class="form-group">
                    <label>Student Name:</label> <span id="feesname"></span>
                    
                 </div>
                 <div class="form-group">
                    <label>Roll:</label> <span id="feesroll"></span>
                    
                 </div>
                 <div class="form-group">
                    <label>Section:</label> <span id="feessec"></span>
                    
                 </div>
                  <div class="form-group">
                    <label>Payment For The Month:</label> 
                    <select class="form-control" id="feesmonth">
                        <option>----select month-----</option>
                        <option id="January" value="January">January</option>
                        <option id="February" value="February">February</option>
                        <option id="March" value="March">March</option>
                        <option id="April" value="April ">April </option>
                        <option id="May" value="May">May</option>
                        <option id="June" value="June">June</option>
                        <option id="July" value="July ">July </option>
                        <option id="August" value="August">August</option>
                        <option id="September" value="September">September</option>
                        <option id="October" value="October">October</option>
                        <option id="November" value="November">November</option>
                        <option id="December" value="December">December</option>
                    </select>
                 </div>
                 <div class="form-group">
                    <label>Amount:<span class="text-danger">*</span></label>
                    <input type="text" class="form-control" id="feesamt"/>   
                 </div>
                 
            </div>
            <div id="pro_err"></div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" id="editFees">Update</button>
          <button type="button" class="btn btn-primary" data-dismiss="modal" id="editmodalclose">Close</button>
        </div>
        
      </div>
      
    </div>
  </div>
<!--  ---------------delete modal class------------------>
  <div class="modal fade" id="delModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Delete Fees Details</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          
        </div>
        <div class="modal-body">
  
            <div class="card-body">
              <input type="hidden" name="id" class="form-control classfield" id="inputdelid">
              <p class="text-center">Do you want to delete?</p>
                 
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" id="delRecord">Delete</button>
          <button type="button" class="btn btn-primary" data-dismiss="modal" id="delmodalclose">Close</button>
        </div>
      </div>
      
    </div>
  </div>

<?php include("inc/footer.php");?>
<script>
  $(function () {
    $("#example1").DataTable({
      "responsive": false,
      "autoWidth": true,
      "info":false,
      "ordering":false,
    });

  });
  
// edit modal action
 
 /* $(document).on('click','#editbtn',function(){
     $('#editModal').modal();
    
     //add data 
    
    var name=$(this).parent().siblings('.std-name').html(); 
    var roll=$(this).parent().siblings('.std-roll').html();
    var section=$(this).parent().siblings('.std-sec').html();
    var amount=$(this).parent().siblings('.std-amt').html();
    var month = $(this).parent().siblings('.std-month').html(); 
    var new_month = month.substring(0,month.indexOf(','));
    
    $("#feesname").html(name);
    $("#feesroll").html(roll);
    $("#feessec").html(section);
    $("#feesamt").val(amount);
    $("#"+new_month).attr("selected",true);
    var eid=$(this).find('.editid').html(); //edit id of student
    $("#modaleditid").val(eid);
  });*/
 
// edit data insert

/*
$(document).on('click','#editFees',function(){
            
            var id=$('#modaleditid').val();// student id
            console.log(id);
            var month=$('#feesmonth').val();
            console.log(month);
            var amount=$('#feesamt').val();
            console.log(amount);
           
            
            $.ajax({
                url:"edit_fees.php",
                type:"post",
                data:{id:id,
                    month:month,
                    amt:amount,
                },
                dataType:'json',
                success:function(data)
                {
                  /* console.log(data);*/
                  /* $('#editmodalclose').trigger('click');
                   $('#alertmsg').slideDown();
                   $('#msg').html(data.msg);
                   $('#msg').css('color','green');*/
                 /*  $('#fees-tbody').DataTable().ajax.reload(null, false);*/

        /*        },
            });
           
        });*/

  

// delete modal
  function deldata($id)
  {
      var did= $id;
     
      $('#inputdelid').val(did)
  };
  
  // remove row from table
  $(document).on('click','#delbtn',function(){
     $('#delModal').modal();
     $(this).closest('tr').addClass('removeRow');
  });
  
  //delete message close
   $(document).on('click','#msgid',function(){
        $("#alertmsg").slideUp();
    });
        
  // delete from dastabase
  $(document).on('click','#delRecord',function()
  {
      var did=$('#inputdelid').val();
       console.log(did);
      $.ajax({
          url:"delete_fees.php",
          type:"post",
          data:{did:did},
          dataType:"json",
          success:function(data)
          {
            console.log(data);
           
           $('#delmodalclose').trigger('click');
           $('.removeRow').animate({
               opacity:0,
           },"slow");
           setTimeout(function(){
            $('.removeRow').remove();   
           },500),
           
           $('#alertmsg').slideDown();
           $('#msg').html(data.msg);
           $('#msg').css('color','red');  
          }
      })
  });
  
</script>
</body>
</html>
