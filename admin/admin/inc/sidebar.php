<?php include("inc/db.php");?>
<?php $reqpage=$_SERVER['REQUEST_URI'];
$final_page = explode('/',$reqpage);
//print_r($final_page);
?>
<!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="dashboard.php" class="brand-link">
      <img src="image/tssv-logo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light"><?php echo $_SESSION['uname'];?></span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
   
   

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column side-menu" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
               <!-- Textlocal SMS Balance -->
    <?php
    	// Account details
    	$apiKey = urlencode('W6fbu4SCs8g-JdWiB4Y4UZEIqMd5lMhWPLiYtuiZrz');
     
    	// Prepare data for POST request
    	$data = 'apikey=' . $apiKey;
     
    	// Send the GET request with cURL
    	$ch = curl_init('https://api.textlocal.in/balance/?' . $data);
    	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    	$response = curl_exec($ch);
    	curl_close($ch);
    	
    	// Process your response here
    	$balance = json_decode($response);
    	
    	//echo $response;
    ?>
      <li class="nav-item mb-2">
          <?php
                if($balance->balance->sms < 100)
                $badge = 'badge-danger';
                if($balance->balance->sms >= 100 && $balance->balance->sms < 500)
                $badge = 'badge-warning';
                if($balance->balance->sms >= 500)
                $badge = 'badge-success';
          ?>
          <span class="badge badge-secondary">SMS Balance:</span> <span class="badge <?php echo $badge; ?>"><?php echo $balance->balance->sms; ?></span>
      </li>
          <li class="nav-item">
	            <a href="dashboard.php" class="nav-link">
	              <i class="nav-icon fas fa-tachometer-alt"></i>
	              <p>
	                Dashboard
	              </p>
	            </a>
	        </li>
        
           <li class="nav-item">
	            <a href="addclass.php" class="nav-link">
	              <i class="nav-icon fas fa-table"></i>
	              <p>
	                 Add Classes

	              </p>
	            </a>
	        </li>
        <li class="nav-item has-treeview">
            <a type="button" class="nav-link dropdown-btn">
              <i class="nav-icon fas fa-users"></i>
              <p>
                Students
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview side-menu students">
              <li class="nav-item">
                <a href="stdprofile.php" class="nav-link add-std">
                  <i class="far fa-user nav-icon"></i>
                  <p>Add Students</p>
                </a>
              </li>
              <li class="nav-item has-treeview">
	            <a href="#" class="nav-link">
	              <i class="nav-icon fas fa-eye"></i>
	              <p>
	                View Students
	                <i class="right fas fa-angle-left"></i>
	              </p>
	            </a>
	            <ul class="nav nav-treeview">
	            	<?php
	                $sel="SELECT * FROM addclass";
	                $rs=$con->query($sel);
	                while($row=$rs->fetch_assoc()){
	                    
	                    $cid=$row['id'];
                        $selc="SELECT count(id) as con FROM studentinfo WHERE class=$cid";
                        $rsc=$con->query($selc);
                        $rowc=mysqli_fetch_array($rsc);
	                ?>

	              <li class="nav-item view_student <?= (strpos($final_page[2],"viewstudent.php") !== false)? "active":""?>">
	                <a href="viewstudent.php?id=<?php echo $row['id'];?>" class="nav-link">
	                  <i class="fas fa-angle-right"></i>
	                  <?php echo $row['cname'];?>  <span class="badge badge-primary" style="float:right;"><?php echo $rowc['con']; ?></span>
	                </a>
	          		 <?php
		                }
		                
		                ?>
	           
	          </li> 
            </ul>
          </li>
        </ul>
        </li>
         <li class="nav-item has-treeview">
            <a type="button" class="nav-link dropdown-btn">
              <i class="fas fa-coins"></i>
              <p>
                Income/Expenditure
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview side-menu">
              <li class="nav-item">
                <a href="income-form.php" class="nav-link income">
                  <i class="fas fa-align-justify"></i>
                  <p>Add Entry</p>
                </a>
              </li>
               <li class="nav-item">
                <a href="balance-sheet.php" class="nav-link income">
                  <i class="fas fa-balance-scale"></i>
                  <p>Balance Sheet</p>
                </a>
              </li>
            </ul>
           </li>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
  

  