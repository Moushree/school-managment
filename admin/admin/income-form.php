<?php

include("inc/db.php");
session_start();
if(!isset($_SESSION['uid']))
{
    header("location:index.php");
}
?>
<!DOCTYPE html>
<html>
<?php include("inc/header.php");?>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  <?php include("inc/navbar.php");
        include("inc/sidebar.php")
  ?>

 <style>
     img.profileimg{
    width: 145px;
    align-items: center;
    margin-left: 88px;
    }
 </style> 

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
       <!--     <h1 class="m-0 text-dark">Student Details Form</h1>-->
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title"> Income/Expenditure Form</h3>

             </div>
            <form id="incomeForm" >
                <div class="card-body">
             
                     <div class="form-group" id="namediv">
                        <label>Select Dropdown:<span class="text-danger">*</span></label>
                        <select class="form-control" id="service">
                            <option value="">-------------</option>
                            <option value="Debit">Debit</option>
                            <option value="Credit">Credit</option>
                        </select>
                       
                      </div>
                        <div class="form-group payment" id="service1"  style="display:none">
                            <label>Pay To:<span class="text-danger">*</span></label>
                            <input type="text" class="form-control"  id="payment1" />
                        </div>  
                      
                         <div class="form-group" id="service2" class="" style="display:none">
                            <label>Received Form:<span class="text-danger">*</span></label>
                            <input type="text" class="form-control payment"  id="payment2" />
                        </div>   
                    <div class="form-group" id="adddiv">
                        <label>Purpose:<span class="text-danger">*</span></label>
                        <textarea class="form-control" id="purpose"></textarea>
                        <span id="errmsg3"></span>
                    </div>
                    <div class="form-group" id="secdiv">
                        <label>Amount:<span class="text-danger">*</span></label>
                        <input type="text" id="amount"  class="form-control"/>
                        
                    </div>
                    <div class="form-group" id="rolldiv">
                        <label>Date:<span class="text-danger">*</span></label>
                        <input type="date" class="form-control" id="paydate" />
                    </div>

                </div>
                <p id="stderrmsg"></p>
                <!-- /.card-body -->
                <div class="card-footer">
                <input type="submit" class="btn btn-primary" id="sub" value="Submit">
                </div>
              </form>
        
      
      </div>
        <div class="alert alert-default" style="display:none;" id="alertmsg">
            <span class="msgclose float-right" id="msgcloseid" style="cursor:pointer;"><i class="fas fa-times"></i></span>
            <strong id="msg"></strong>
        </div>
    </section>
    <!-- /.content -->
  </div>
  
<!-------------style for form validation---------------------->

<style>

.error_form {
    border: 2px solid red;
}
.error {
  color: red;
  margin-left: 5px;
}

label.error {
  display: inline;
  
}
</style>


<?php include("inc/footer.php");?>

<script>



$('select').on('change', function() {
  var service=$(this).val();
  
  if(service=="")
  {
     $('#service1').slideUp(); 
     $('#service2').slideUp();
  }
  else if(service=="Debit"){
      $('#service1').slideDown();
      $('#service2').slideUp();
  }
  else{
    $('#service2').slideDown(); 
    $('#service1').slideUp();
  }
});

$('#incomeForm').on('submit',function(e){
  
  e.preventDefault();
  var service = $('#service').val();
  var payment = (service=='Debit')? $('#payment1').val():$('#payment2').val();
  console.log(payment);
  var purpose = $('#purpose').val();
  var amount = $('#amount').val();
  var paydate = $('#paydate').val();
  
   
        // form validation
            
        $(".error").remove();
        $("#incomeForm").removeClass('error_form')
    
        if (service.length < 1) {
          $('#service').after('<span class="error">This field is required</span>');
          $('#incomeForm').addClass('error_form');
          $('html, body').animate({
                scrollTop: $("#namediv").offset().top
            }, 500);
          return false;
        }
        if (payment.length < 1) {
          $('.payment').after('<span class="error">This field is required</span>');
          $('#incomeForm').addClass('error_form');
          $('html, body').animate({
                scrollTop: $("#gnamediv").offset().top
            }, 500);
          return false;
        }

        if (purpose.length < 1) {
          $('#purpose').after('<span class="error">This field is required</span>');
          $('#incomeForm').addClass('error_form');
          $('html, body').animate({
                scrollTop: $("#datediv").offset().top
            }, 500);
          return false;
        }
         if (amount.length < 1) {
          $('#amount').after('<span class="error">This field is required</span>');
          $('#incomeForm').addClass('error_form');
          $('html, body').animate({
                scrollTop: $("#adddiv").offset().top
            }, 500);
          return false;
        }
        if (paydate.length < 1) {
          $('#paydate').after('<span class="error">This field is required</span>');
          $('#incomeForm').addClass('error_form');
          $('html, body').animate({
                scrollTop: $("#phdiv").offset().top
            }, 500);
          return false;
        }

  $.ajax({
      url:'income-form-ins.php',
      type:'post',
      data:{ service:service,
              payment:payment,
              purpose:purpose,
              amount:amount,
              paydate:paydate
      },
      dataType:'json',
      success:function(data)
      {
          console.log(data);
          $('#alertmsg').slideDown();
          $('#msg').html(data.msg);
          $('#msg').css('color','green');
          $('#incomeForm')[0].reset();
      }
      
  });
  
});

$(document).on('click','#msgcloseid',function(){
    $('#alertmsg').slideUp();
})
    
</script>
</body>
</html>
