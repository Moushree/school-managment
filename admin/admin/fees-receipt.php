<?php
include('mpdf/vendor/autoload.php');

include('inc/db.php');

$html='<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Tethibari Sarada Sishu Vidyamandir</title>
    <link rel="stylesheet" href="balancecss/style.css" media="all" />
  </head>
  <body>';
  
  $sid=$_POST['feeid'];// student id
  $feemonth=$_POST['feemonth'];
  $sel="SELECT fees_table.*, studentinfo.sname FROM fees_table LEFT JOIN studentinfo ON fees_table.sid=studentinfo.id WHERE fees_table.sid=$sid AND fees_table.month='$feemonth' ";
  $res=$con->query($sel);
  $row=$res->fetch_assoc();
 $html.='<main>
 
    <div  style="width:47%;  padding-left:20px;padding-top:30px;padding-right:2px; float:left;" >
    
      <div style="text-align:center;  border:1px solid #000; padding-bottom:10px;">
     
        <div>Receipt(Student Copy)</div>
        <div style="font-size:16px;"><b>TETHIBARI SARADA SISHU VIDYAMANDIR</b></div>
        <div style="font-size:14px; font-style:italic;">[Co-Education Nursery School Upto Standard-VI]</div>
        <div style="font-size:14px;">Estd. - 1998::Regd. No -S/IL/48426 of 2007-08</div>
        <div style="font-size:14px;">Tethibari || Kismat Bajkul || Purba Medinipur</div>
        
      </div>
      <div style="padding-top:10px; ">
      <div style="text-align:left;  border:1px solid #000;">
        <div style="padding:10px 0px 0px 0px">
            <div style="width:45%; float:left;  font-size:12px; padding-left:5px; "><b>RECNO: R-6277</b></div>
            <div  style="width:50%; float:right; font-size:12px" ><b>RECEIPT DATE:' .date('d/m/Y').'</b></div>
        </div>
        <div style="padding:5px 0px 0px 0px">
            <div style="width:45%; float:left;  font-size:12px; padding-left:5px;"><b>REG.NO: TSSV-2018-494</b></div>
            <div  style="width:50%; float:right; font-size:12px; text-transform: capitalize;" ><b>MONTH Of: '.$row['month'].'</b></div>
        </div>
        <div style="padding:5px 0px 0px 0px">
            <div style="width:45%; float:left;  font-size:12px; padding-left:5px; text-transform: capitalize;"><b>NAME: '.$row['sname'].'</b></div>
        </div>
      </div>
      </div>
      <div  style="padding-top:10px; ">
       <div style="border:1px solid #000; padding:10px 0px 10px 0px">
	     <div style="width:70%; float:left; padding-left:20px; font-size:12px"><b>PERTICULARS</b></div>
	     <div  style="width:24%; float:right; font-size:12px" ><b>AMOUNT</b></div>
      </div>
      <div id="feereceipt" style="border:1px solid #000;">
          <div style="padding:10px 0px 5px 0px">
    	     <div style="width:70%; float:left; padding-left:20px; font-size:12px">TUTION FEES</div>
    	     <div  style="width:24%; float:right; font-size:12px" >'.$row['tfee'].'</div>
          </div>
                   <div style="padding:0px 0px 5px 0px">
    	     <div style="width:70%; float:left; padding-left:20px; font-size:12px">DEVELOPMENT FEE</div>';
    	     if(!$row['dfee']){
    	         $html.='<div  style="width:24%; float:right; font-size:12px" >0</div>';
    	     }else{
    	        $html.='<div  style="width:24%; float:right; font-size:12px">'.$row['dfee'].'</div>'; 
    	     }
    	     
          $html.='</div>
          <div style="padding:0px 0px 5px 0px">
    	     <div style="width:70%; float:left; padding-left:20px; font-size:12px">ADDMISSION/TRANSFER</div>';
    	     if(!$row['addmission']){
    	         $html.='<div  style="width:24%; float:right; font-size:12px" >0</div>';
    	     }else{
    	        $html.='<div  style="width:24%; float:right; font-size:12px">'.$row['addmission'].'</div>'; 
    	     }
    	     
          $html.='</div>
         <div style="padding:0px 0px 5px 0px">
    	     <div style="width:70%; float:left; padding-left:20px; font-size:12px">GAMES & SPORTS</div>';
    	     if(!$row['game']){
    	         $html.='<div  style="width:24%; float:right; font-size:12px" >0</div>';
    	     }else{
    	        $html.='<div  style="width:24%; float:right; font-size:12px">'.$row['game'].'</div>'; 
    	     }
    	     
          $html.='</div>
           <div style="padding:0px 0px 5px 0px">
    	     <div style="width:70%; float:left; padding-left:20px; font-size:12px">SOCIAL FEE</div>';
    	     if(!$row['sfee']){
    	         $html.='<div  style="width:24%; float:right; font-size:12px" >0</div>';
    	     }else{
    	        $html.='<div  style="width:24%; float:right; font-size:12px">'.$row['sfee'].'</div>'; 
    	     }
    	     
          $html.='</div>
           <div style="padding:0px 0px 5px 0px">
    	     <div style="width:70%; float:left; padding-left:20px; font-size:12px">MAGAZINE FEE</div>';
    	     if(!$row['mfee']){
    	         $html.='<div  style="width:24%; float:right; font-size:12px" >0</div>';
    	     }else{
    	        $html.='<div  style="width:24%; float:right; font-size:12px">'.$row['mfee'].'</div>'; 
    	     }
    	     
          $html.='</div>
          <div style="padding:0px 0px 5px 0px">
    	     <div style="width:70%; float:left; padding-left:20px; font-size:12px">ELECTRICITY</div>';
    	     if(!$row['ele']){
    	         $html.='<div  style="width:24%; float:right; font-size:12px" >0</div>';
    	     }else{
    	        $html.='<div  style="width:24%; float:right; font-size:12px">'.$row['ele'].'</div>'; 
    	     }
    	     
          $html.='</div>
          <div style="padding:0px 0px 5px 0px">
    	     <div style="width:70%; float:left; padding-left:20px; font-size:12px">ESTABLISHMENT FEE</div>';
    	     if(!$row['est']){
    	         $html.='<div  style="width:24%; float:right; font-size:12px" >0</div>';
    	     }else{
    	        $html.='<div  style="width:24%; float:right; font-size:12px">'.$row['est'].'</div>'; 
    	     }
    	     
          $html.='</div>
          <div style="padding:0px 0px 5px 0px">
    	     <div style="width:70%; float:left; padding-left:20px; font-size:12px">COMPUTER FEE</div>';
    	     if(!$row['cfee']){
    	         $html.='<div  style="width:24%; float:right; font-size:12px" >0</div>';
    	     }else{
    	        $html.='<div  style="width:24%; float:right; font-size:12px">'.$row['cfee'].'</div>'; 
    	     }
    	     
          $html.='</div>
          <div style="padding:0px 0px 5px 0px">
    	     <div style="width:70%; float:left; padding-left:20px; font-size:12px">EXAM FEE</div>';
    	     if(!$row['efee']){
    	         $html.='<div  style="width:24%; float:right; font-size:12px" >0</div>';
    	     }else{
    	        $html.='<div  style="width:24%; float:right; font-size:12px">'.$row['efee'].'</div>'; 
    	     }
    	     
          $html.='</div>
          <div style="padding:0px 0px 5px 0px">
    	     <div style="width:70%; float:left; padding-left:20px; font-size:12px">TOUR/PICNIC</div>';
    	     if(!$row['tour']){
    	         $html.='<div  style="width:24%; float:right; font-size:12px" >0</div>';
    	     }else{
    	        $html.='<div  style="width:24%; float:right; font-size:12px">'.$row['tour'].'</div>'; 
    	     }
    	     
          $html.='</div>
          <div style="padding:0px 0px 5px 0px">
    	     <div style="width:70%; float:left; padding-left:20px; font-size:12px">SCHOOL VAN FEE</div>';
    	     if(!$row['vfee']){
    	         $html.='<div  style="width:24%; float:right; font-size:12px" >0</div>';
    	     }else{
    	        $html.='<div  style="width:24%; float:right; font-size:12px">'.$row['vfee'].'</div>'; 
    	     }
    	     
          $html.='</div>
          <div style="padding:0px 0px 5px 0px">
    	     <div style="width:70%; float:left; padding-left:20px; font-size:12px">OTHER CHARGES</div>';
    	     if(!$row['other']){
    	         $html.='<div  style="width:24%; float:right; font-size:12px" >0</div>';
    	     }else{
    	        $html.='<div  style="width:24%; float:right; font-size:12px">'.$row['other'].'</div>'; 
    	     }
    	     
          $html.='</div>
          <div style="padding:0px 0px 5px 0px">
    	     <div style="width:70%; float:left; padding-left:20px; font-size:12px">TOTAL FEE</div>
    	     <div  style="width:24%; float:right; font-size:12px" >'.$row['total'].'</div>
          </div>
       </div>
      </div>
        <div style="padding-top:10px; ">
          <div style="text-align:left; ">
            <div style="padding:10px 0px 0px 0px">
                <div style="width:45%; float:left;  font-size:12px; padding-left:5px; ">Cashier</div>
                <div  style="width:50%; float:right; font-size:12px" >Rector/HM</div>
            </div>
          </div>
          </div>  
    </div>
    <div style="width:47%;  padding-right:5px;padding-top:30px; padding-left:2px;  float:right;">
          <div style="text-align:center;  border:1px solid #000; padding-bottom:10px;">
     
        <div>Receipt(School Copy)</div>
        <div style="font-size:16px;"><b>TETHIBARI SARADA SISHU VIDYAMANDIR</b></div>
        <div style="font-size:14px; font-style:italic;">[Co-Education Nursery School Upto Standard-VI]</div>
        <div style="font-size:14px;">Estd. - 1998::Regd. No -S/IL/48426 of 2007-08</div>
        <div style="font-size:14px;">Tethibari || Kismat Bajkul || Purba Medinipur</div>
        
      </div>
      <div style="padding-top:10px; ">
      <div style="text-align:left;  border:1px solid #000;">
        <div style="padding:10px 0px 0px 0px">
            <div style="width:45%; float:left;  font-size:12px; padding-left:5px; "><b>RECNO: R-6277</b></div>
            <div  style="width:50%; float:right; font-size:12px" ><b>RECEIPT DATE:' .date('d-M-Y').'</b></div>
        </div>
        <div style="padding:5px 0px 0px 0px">
            <div style="width:45%; float:left;  font-size:12px; padding-left:5px;"><b>REG.NO: TSSV-2018-494</b></div>
            <div  style="width:50%; float:right; font-size:12px; text-transform: capitalize;" ><b>MONTH Of: '.$row['month'].'</b></div>
        </div>
        <div style="padding:5px 0px 0px 0px">
            <div style="width:45%; float:left;  font-size:12px; padding-left:5px; text-transform: capitalize;"><b>NAME:  '.$row['sname'].'</b></div>
        </div>
      </div>
      </div>
      <div  style="padding-top:10px; ">
       <div style="border:1px solid #000; padding:10px 0px 10px 0px">
	     <div style="width:70%; float:left; padding-left:20px; font-size:12px"><b>PERTICULARS</b></div>
	     <div  style="width:24%; float:right; font-size:12px" ><b>AMOUNT</b></div>
      </div>
      <div id="feereceipt" style="border:1px solid #000;">
          <div style="padding:10px 0px 5px 0px">
    	     <div style="width:70%; float:left; padding-left:20px; font-size:12px">TUTION FEES</div>
    	     <div  style="width:24%; float:right; font-size:12px" >'.$row['tfee'].'</div>
          </div>
                   <div style="padding:0px 0px 5px 0px">
    	     <div style="width:70%; float:left; padding-left:20px; font-size:12px">DEVELOPMENT FEE</div>';
    	     if(!$row['dfee']){
    	         $html.='<div  style="width:24%; float:right; font-size:12px" >0</div>';
    	     }else{
    	        $html.='<div  style="width:24%; float:right; font-size:12px">'.$row['dfee'].'</div>'; 
    	     }
    	     
          $html.='</div>
          <div style="padding:0px 0px 5px 0px">
    	     <div style="width:70%; float:left; padding-left:20px; font-size:12px">ADDMISSION/TRANSFER</div>';
    	     if(!$row['addmission']){
    	         $html.='<div  style="width:24%; float:right; font-size:12px" >0</div>';
    	     }else{
    	        $html.='<div  style="width:24%; float:right; font-size:12px">'.$row['addmission'].'</div>'; 
    	     }
    	     
          $html.='</div>
         <div style="padding:0px 0px 5px 0px">
    	     <div style="width:70%; float:left; padding-left:20px; font-size:12px">GAMES & SPORTS</div>';
    	     if(!$row['game']){
    	         $html.='<div  style="width:24%; float:right; font-size:12px" >0</div>';
    	     }else{
    	        $html.='<div  style="width:24%; float:right; font-size:12px">'.$row['game'].'</div>'; 
    	     }
    	     
          $html.='</div>
           <div style="padding:0px 0px 5px 0px">
    	     <div style="width:70%; float:left; padding-left:20px; font-size:12px">SOCIAL FEE</div>';
    	     if(!$row['sfee']){
    	         $html.='<div  style="width:24%; float:right; font-size:12px" >0</div>';
    	     }else{
    	        $html.='<div  style="width:24%; float:right; font-size:12px">'.$row['sfee'].'</div>'; 
    	     }
    	     
          $html.='</div>
           <div style="padding:0px 0px 5px 0px">
    	     <div style="width:70%; float:left; padding-left:20px; font-size:12px">MAGAZINE FEE</div>';
    	     if(!$row['mfee']){
    	         $html.='<div  style="width:24%; float:right; font-size:12px" >0</div>';
    	     }else{
    	        $html.='<div  style="width:24%; float:right; font-size:12px">'.$row['mfee'].'</div>'; 
    	     }
    	     
          $html.='</div>
          <div style="padding:0px 0px 5px 0px">
    	     <div style="width:70%; float:left; padding-left:20px; font-size:12px">ELECTRICITY</div>';
    	     if(!$row['ele']){
    	         $html.='<div  style="width:24%; float:right; font-size:12px" >0</div>';
    	     }else{
    	        $html.='<div  style="width:24%; float:right; font-size:12px">'.$row['ele'].'</div>'; 
    	     }
    	     
          $html.='</div>
          <div style="padding:0px 0px 5px 0px">
    	     <div style="width:70%; float:left; padding-left:20px; font-size:12px">ESTABLISHMENT FEE</div>';
    	     if(!$row['est']){
    	         $html.='<div  style="width:24%; float:right; font-size:12px" >0</div>';
    	     }else{
    	        $html.='<div  style="width:24%; float:right; font-size:12px">'.$row['est'].'</div>'; 
    	     }
    	     
          $html.='</div>
          <div style="padding:0px 0px 5px 0px">
    	     <div style="width:70%; float:left; padding-left:20px; font-size:12px">COMPUTER FEE</div>';
    	     if(!$row['cfee']){
    	         $html.='<div  style="width:24%; float:right; font-size:12px" >0</div>';
    	     }else{
    	        $html.='<div  style="width:24%; float:right; font-size:12px">'.$row['cfee'].'</div>'; 
    	     }
    	     
          $html.='</div>
          <div style="padding:0px 0px 5px 0px">
    	     <div style="width:70%; float:left; padding-left:20px; font-size:12px">EXAM FEE</div>';
    	     if(!$row['efee']){
    	         $html.='<div  style="width:24%; float:right; font-size:12px" >0</div>';
    	     }else{
    	        $html.='<div  style="width:24%; float:right; font-size:12px">'.$row['efee'].'</div>'; 
    	     }
    	     
          $html.='</div>
          <div style="padding:0px 0px 5px 0px">
    	     <div style="width:70%; float:left; padding-left:20px; font-size:12px">TOUR/PICNIC</div>';
    	     if(!$row['tour']){
    	         $html.='<div  style="width:24%; float:right; font-size:12px" >0</div>';
    	     }else{
    	        $html.='<div  style="width:24%; float:right; font-size:12px">'.$row['tour'].'</div>'; 
    	     }
    	     
          $html.='</div>
          <div style="padding:0px 0px 5px 0px">
    	     <div style="width:70%; float:left; padding-left:20px; font-size:12px">SCHOOL VAN FEE</div>';
    	     if(!$row['vfee']){
    	         $html.='<div  style="width:24%; float:right; font-size:12px" >0</div>';
    	     }else{
    	        $html.='<div  style="width:24%; float:right; font-size:12px">'.$row['vfee'].'</div>'; 
    	     }
    	     
          $html.='</div>
          <div style="padding:0px 0px 5px 0px">
    	     <div style="width:70%; float:left; padding-left:20px; font-size:12px">OTHER CHARGES</div>';
    	     if(!$row['other']){
    	         $html.='<div  style="width:24%; float:right; font-size:12px" >0</div>';
    	     }else{
    	        $html.='<div  style="width:24%; float:right; font-size:12px">'.$row['other'].'</div>'; 
    	     }
    	     
          $html.='</div>
          <div style="padding:0px 0px 5px 0px">
    	     <div style="width:70%; float:left; padding-left:20px; font-size:12px">TOTAL FEE</div>
    	     <div  style="width:24%; float:right; font-size:12px" >'.$row['total'].'</div>
          </div>
       </div>
      </div>
    <div style="padding-top:10px; ">
      <div style="text-align:left; ">
        <div style="padding:10px 0px 0px 0px">
            <div style="width:45%; float:left;  font-size:12px; padding-left:5px; ">Cashier</div>
            <div  style="width:50%; float:right; font-size:12px" >Rector/HM</div>
        </div>
      </div>
      </div> 
    </div>
      
     
    </main>

  </body>
</html>
';
$mpdf = new \Mpdf\Mpdf([
    'format' => 'A4',
    'margin_left' => 0,
    'margin_right' => 5,
    'margin_top' => 0,
    'margin_bottom' => 0,
    'margin_header' => 0,
    'margin_footer' => 0,
]);


    
$mpdf->AddPage();


$css=file_get_contents('balancecss/style.css');
$mpdf->WriteHTML($css,1);
$mpdf->WriteHTML($html);
$mpdf->Output('Fees-receipt'.date('d-m-Y_H_i_s').'.pdf',D);
exit;
?>