<?php
include("inc/db.php");
session_start();
if(!isset($_SESSION['uid']))
{
    header("location:index.php");
}
?>
<!DOCTYPE html>
<html>
<?php include("inc/header.php");?>
<style>
  .credit-row{
    background-color: rgb(157 222 168 / 53%);
  }  
  .debit-row{
    background-color: rgb(222 166 157 / 53%);
  }
</style>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  <?php include("inc/navbar.php");
        include("inc/sidebar.php")
  ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">



    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
    <div class="alert alert-default" style="display:none;" id="alertmsg">
        <span class="msgclose float-right" id="msgid" style="cursor:pointer;"><i class="fas fa-times"></i></span>
        <strong id="msg"></strong>
    </div>
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <div class="row">
                    <div class="col-md-6">
                      <h3 class="card-title">Income / Expenditure Table</h3>  
                    </div>
                    <div class="col-md-6">
                        <form id="downForm">
                            
                          <p>Download Balance-Sheet: 
                         <!-- ----------select month for download-------------->
                        <span><select id="month_id" required>
                                
                                <option value="">-----Month-----</option>
                                <option value="January">January</option>
                                <option value="February">February</option>
                                <option value="March">March</option>
                                <option value="April ">April </option>
                                <option value="May">May</option>
                                <option value="June">June</option>
                                <option value="July ">July </option>
                                <option value="August">August</option>
                                <option value="September">September</option>
                                <option value="October">October</option>
                                <option value="November">November</option>
                                <option value="December">December</option>
                                     </select></span>
                             <span>
                            <!-- ----------select year for download-------------->
                                 <select id="year_id" required>
                                     <option value="">--year--</option>
                                     <?php
                                    $sely="SELECT DISTINCT Year(paydate) as pay_y FROM account";
                                    $rsy=$con->query($sely);
                                     while($rowy=$rsy->fetch_assoc())
                                    {
                                        
                                    ?>
                                     <option value="<?php echo $rowy['pay_y'];?>"><?php echo $rowy['pay_y'];?></option>
                                    <?php
                                    }
                                    ?> 
                                 </select>
                                 
                             </span><span><button type="submit" class="btn btn-link btn-sm" id="downbtn"><i class="fas fa-download"></i></button></span></p>  
                        </form>
                      <form id="pdf_form" action="balancepdf.php" method="post">
                         <input type="hidden" name="month" id="form_mon">
                         <input type="hidden" name="year" id="form_year"> 
                      </form>
                    </div>
                </div>
                
              </div>
              <!-- /.card-header -->
              <div class="card-body table-responsive">
                <table id="example1" class="table table-bordered ">
                  <thead>
                  <tr>
                    <th></th>
                    <th>Pay to/ Received From</th>
                    <th>Purpose</th>
                    <th>Amount</th>
                    <th>Date</th>
                    <th>Delete</th>
                  </tr>
                  </thead>
                  <tbody>
                    <?php
                    $sel="SELECT * FROM account ORDER BY paydate DESC";
                    $rs=$con->query($sel);
                     while($row=$rs->fetch_assoc())
                    {
                    ?>
                    <tr class="<?= ($row['status']==1)?'credit-row':'debit-row'?>">
                    <td><?php echo $row['service'];?></td>
                    <td><?php echo $row['p_name'];?></td>
                    <td><?php echo $row['purpose'];?></td>
                    <td><?php echo $row['amount'];?></td>
                    <?php
                         $pay_dt = new DateTime($row['paydate']);
                    ?>
                    <td><?php echo $pay_dt->format('d-F-Y');?></td>

                    <td><a  type="button" id="delbtn" class="text-center text-danger"  data-toggle="tooltip" title="Delete"><span class="delid d-none"><?php echo $row['id'];?></span><i class="far fa-trash-alt" aria-hidden="true"></i></a></td>
                   </tr>
                    <?php
                    }
                   
                    ?> 
                   
                    
                    

                  </tbody>
                
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
<!--  ---------------pdf alert modal class------------------>
  <div class="modal fade" id="pdfModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Delete classes</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          
        </div>
        <div class="modal-body">
  
            <div class="card-body">
              
              <p class="text-center" id="pdfalt">Sorry! Data Is Not In Record</p>
                 
            </div>
        </div>
        <div class="modal-footer">
          
          <button type="button" class="btn btn-primary" data-dismiss="modal" id="pdfmodalclose">Close</button>
        </div>
      </div>
      
    </div>
  </div>
<!--  ---------------delete modal class------------------>
  <div class="modal fade" id="delModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
           <input type="hidden" id="inputdelid"> 
          <h4 class="modal-title">Delete classes</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          
        </div>
        <div class="modal-body">
  
            <div class="card-body">
              <input type="hidden" name="id" class="form-control classfield" id="inputdelid">
              <p class="text-center">Do you want to delete?</p>
                 
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" id="delRecord">Delete</button>
          <button type="button" class="btn btn-primary" data-dismiss="modal" id="delmodalclose">Close</button>
        </div>
      </div>
      
    </div>
  </div>

<?php include("inc/footer.php");?>
<script>
 $(function () {
    $("#example1").DataTable({
      "responsive": false,
      "autoWidth": true,
      "info":false,
      "ordering": false,
    });

  });
  
 //download pdf form 
 $('#downForm').on('submit',function(e){
      
     e.preventDefault();
      var month=$('#month_id').val();
      var year=$('#year_id').val();
      console.log(month);
      console.log(year);
      
      $.ajax({
        
        url:'balance-check.php',
        type:'post',
        data:{
            month:month,
            year:year,
        },
        dataType:'json',
        success:function(data){
           console.log(data); 
           if(data.status==1){
               $('#form_mon').val(data.month);
               $('#form_year').val(data.year);
               $('#downForm')[0].reset();
               $('#pdf_form').submit();
              
           }
           else{
               $('#pdfModal').modal();
               $('#downForm')[0].reset();
           }
        }
      });
  });
  
  // remove row from table
  $(document).on('click','#delbtn',function(){
      
     var did=$(this).find(".delid").html();// student id
     console.log(did);
     $('#inputdelid').val(did);
     
     $('#delModal').modal();
     $(this).closest('tr').addClass('removeRow');
     

  });
  
  //delete message close
   $(document).on('click','#msgid',function(){
        $("#alertmsg").slideUp();
    });
        
  // delete from dastabase
 $(document).on('click','#delRecord',function()
  {
      var did=$('#inputdelid').val();
       console.log(did);
           $.ajax({
          url:"delbalance.php",
          type:"post",
          data:{did:did},
          dataType:"json",
          success:function(data)
          {
            console.log(data);
           
           $('#delmodalclose').trigger('click');
           $('.removeRow').animate({
               opacity:0,
           },"slow");
           setTimeout(function(){
            $('.removeRow').remove();   
           },500),
           
           $('#alertmsg').slideDown();
           $('#msg').html(data.msg);
           $('#msg').css('color','red');  
          }
      })
  });
  
</script>
</body>
</html>
