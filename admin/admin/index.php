<?php
session_start();
include("inc/db.php");

if(isset($_POST['sub']))
{
    $u=mysqli_real_escape_string($con,trim($_POST['user']));
    $solt="a2e48ae5d8524e83c78d2abf991df649";
    $p=mysqli_real_escape_string($con,trim($solt.md5($_POST['password'])));
    
    $sel="SELECT * FROM admin WHERE (name='$u' OR email='$u') AND password='$p'";
    $rs=$con->query($sel);
    if($rs->num_rows>0)
    {
        while($row=$rs->fetch_assoc())
       {
        $_SESSION['uid']=$row['id'];
        $_SESSION['uname']=$row['name']; 
        header("location:dashboard.php");
       }
    }
    else
    {
       $msg="Incorrect Username/Password";
      
    }
}

if(isset($_SESSION['uid']))
{
    header("location:dashboard.php");
}
?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Tethibari Sarada Sishu Vidyamandir - Login</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <img src="image/tssv-logo.png"><br>
    <p class="text-center">Estd. - 1998</p>
  </div>
 
  <!-- /.login-logo -->
  <div class="card">
    <div class="card-body login-card-body">
      <p class="login-box-msg">Login Form</p>

      <form action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>" method="post">
        <?php
        if(isset($msg))
        {
        ?>
        <p>
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-label="close">&times;</button>
            <strong><?php echo $msg?></strong>
        </div>
        </p>
        <?php
        }
        ?>
         <span id="uerr"></span>
        <div class="input-group mb-3">
         
          <input type="text" class="form-control" id="user" name="user" placeholder="Email/Username">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-user"></span>
            </div>
          </div>
          
        </div>
        <span id="perr"></span>
        <div class="input-group mb-3">
          <input type="password" class="form-control" id="password" name="password" placeholder="Password">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
         
 
        <input type="submit" id="login" name="sub" class="btn btn-primary btn-user btn-block" value="Login">
       
        
      </form>

 
      <!-- /.social-auth-links -->

      <p class="text-center">
        <a href="forgot-password.html">I forgot my password</a>
      </p>
    
    </div>
    <!-- /.login-card-body -->
  </div>
</div>
<!-- /.login-box -->

<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
    <script type="text/javascript">
         $(document).ready(function(){
            
            $('#login').click(function(e){
               
               
                var user= $('#user').val();
                var password=$('#password').val();
                if(user == "")
                {
                    $('#uerr').html('** Username/Email must be filled');
                    $('#uerr').css('color','red');
                    return false;
                }
                 if(password == "")
                {
                    $('#perr').html('** Password  must be field');
                    $('#perr').css('color','red');
                    return false;
                }
            });
  
    
    });
        
    </script>
</body>
</html>
