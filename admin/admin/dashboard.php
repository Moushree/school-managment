<?php

include("inc/db.php");

session_start();
if(!isset($_SESSION['uid'])){
    header("location:index.php");
}
?>
<!DOCTYPE html>
<html>
<?php include("inc/header.php");?>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  <?php include("inc/navbar.php");
        include("inc/sidebar.php")
  ?>

  

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Dashboard</h1>
          </div><!-- /.col -->
         <!-- <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard v1</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
          
          <div class="row">
     <?php
       $sel="SELECT * FROM addclass";
       $rs=$con->query($sel);
       while($row=$rs->fetch_assoc()){
       ?>
        <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-warning">
              <div class="inner">
                <?php
                $cid=$row['id'];
                $selc="SELECT count(id) as con FROM studentinfo WHERE class=$cid";
                $rsc=$con->query($selc);
                $rowc=mysqli_fetch_array($rsc);
                ?>
                <h4><?php echo $row['cname'];?></h4>

                <p><?= $rowc['con'];?></p>
              </div>
              <div class="icon">
                <i class="fas fa-users"></i>
              </div>
              <a href="viewstudent.php?id=<?php echo $row['id'];?>" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
         
        <?php
       }
      ?>
    </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
<?php include("inc/footer.php");?>

</body>
</html>
