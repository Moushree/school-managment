<?php

include("inc/db.php");
session_start();
if(!isset($_SESSION['uid']))
{
    header("location:index.php");
}
?>
<!DOCTYPE html>
<html>
<?php include("inc/header.php");?>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  <?php include("inc/navbar.php");
        include("inc/sidebar.php")
  ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">



    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
    <div class="alert alert-default" style="display:none;" id="alertmsg">
        <span class="msgclose float-right" id="msgid" style="cursor:pointer;"><i class="fas fa-times"></i></span>
        <strong id="msg"></strong>
    </div>
        <div class="row">
          <div class="col-12">

            <?php
                    $cid=$_GET['id'];
                    $selc="SELECT * FROM addclass WHERE id='$cid'";
                    $rsc=$con->query($selc);
                    while($rowc=$rsc->fetch_assoc())
                    {
                        $classid= $rowc['id'];
                        $classname = $rowc['cname'];
                    }
                    
                    ?>

            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Students: <?php echo $classname; ?></h3>
                <a href="feesdetails.php?id=<?php echo $classid?>" class="btn btn-primary float-right text-white">Payment Details</a>
              </div>
              <!-- /.card-header -->
              <div class="card-body table-responsive">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th></th>
                    <th>Name</th>
                    <th>Roll</th>
                    <th>Section</th>
                    <!--<th>Gender</th>
                    <th>Blood Group</th>
                    <th>Guardian Name</th>
                    <th>Address</th>
                    <th>Phone</th>
                    <th>Phone(Optinal)</th>
                    <th>Email</th>-->
                    <th>View</th>
                    <th>Edit</th>
                    <th>Promotion</th>
                    <th>Fees Payment</th>
                    <th>Delete</th>
                  </tr>
                  </thead>
                  <tbody>
                      
                    <?php
                    $id=$_GET['id'];
                    $sel="SELECT * FROM studentinfo where class='$id' ORDER BY  concat(section) asc , roll ";
                    $rs=$con->query($sel);
                    while($row=$rs->fetch_assoc())
                    {
                    ?>
                   <tr>
                    <td>
                        <?php 
                        $image=$row['image'];
                        if($image=="")
                        {
                              if($row['gender'] == 'Male'){
                            ?>
                             <img width="75px" src="../admin/image/profile-male.png">
                             <?php
                             }else{
                             ?>
                             <img width="75px" src="../admin/image/profile-female.png">
                            <?php
                             }   
                        } else{
                        ?>
                        <img width="75px" src="../admin/studentimg/<?php echo $row['image'];?>">
                        <?php
                        }
                        ?>

                    </td>
                    <td><?php echo $row['sname'];?></td>
                    <td><?php echo $row['roll'];?></td>
                    <td><?php echo $row['section'];?></td>
                    <!--<td><?php echo $row['gender'];?></td>
                    <td><?php echo $row['blood'];?></td>
                    <td><?php echo $row['gname'];?></td>
                    <td><?php echo $row['address'];?></td>
                    <td><?php echo $row['phone1'];?></td>
                    <td><?php echo $row['phone2'];?></td>
                    <td><?php echo $row['email'];?></td>-->
                    <td><a href="viewstudentsingle.php?id=<?php echo $row['id'] ?>" class="text-success text-center"><i class="far fa-eye" aria-hidden="true"></i></a></td>
                    <td><a href="editprofile.php?id=<?php echo $row['id'];?>" class="text-center"><i class="fas fa-edit" aria-hidden="true"></i></a></td>
                    <td><a  type="button" onclick="promotion(<?php echo $row['id'];?>)" id="probtn" class="text-center text-success"><i class="fa fa-trophy" aria-hidden="true"></i></a></td>
                    <td><a href="fees-payment.php?id=<?php echo $row['id'] ?>"  class="text-center text-primary"><i class="fas fa-money-bill" aria-hidden="true"></i></a></td>
                    <td><a  type="button"  onclick="delprofile(<?php echo $row['id'];?>)" id="delbtn" class="text-center text-danger"><i class="far fa-trash-alt" aria-hidden="true"></i></a></td>
                   </tr>
                    <?php
                    }
                    
                    ?>

                  </tbody>
                
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
<!-- -------------------promotion modal ------------------->
 
 <div class="modal fade" id="proModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Promote Class</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          
        </div>
        <div class="modal-body">
  
            <div class="card-body">
              <input type="hidden" name="id" class="form-control classfield" id="inputproid">
                 <div class="form-group">
                    <label>Student Name:</label> <span id="stdname"></span>
                    
                 </div>
                 <div class="form-group">
                    <label>Roll:</label>
                    <input type="text" id="stdroll" class="form-control"/>
                 </div>
                 <div class="form-group">
                    <label>Enter Class:<span class="text-danger">*</span></label>
                        <select class="form-control" id="stdclass" name="stdclass">
                            
                        </select>
                    
                  </div>
                  <div class="form-group">
                    <label>Section</label> 
                    <input type="text" id="stdsec" class="form-control" />
                 </div>
                 
            </div>
            <div id="pro_err"></div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" id="proRecord">Promoted</button>
          <button type="button" class="btn btn-primary" data-dismiss="modal" id="promodalclose">Close</button>
        </div>
        
      </div>
      
    </div>
  </div>

<!-- -------------------fees modal ------------------->
 
<!-- <div class="modal fade" id="feesModal" role="dialog">
    <div class="modal-dialog">-->
    
      <!-- Modal content-->
     <!-- <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Pay fees</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          
        </div>
        <div class="modal-body">
  
            <div class="card-body">
              <input type="hidden" name="id" class="form-control classfield" id="feesid">
                 <div class="form-group">
                    <label>Student Name:</label> <span id="feesname"></span>
                    
                 </div>
                 <div class="form-group">
                    <label>Roll:</label> <span id="feesroll"></span>
                    
                 </div>
                 <div class="form-group">
                    <label>Class:</label> <span id="feesclass"></span>
                    
                 </div>
                 <div class="form-group">
                    <label>Section:</label> <span id="feessec"></span>
                    
                 </div>
                   <div class="form-group">
                    <label>Payment For The Month:</label> 
                    <select class="form-control" id="feesmonth">
                        <option>----select month-----</option>
                        <option value="January">January</option>
                        <option value="February">February</option>
                        <option value="March">March</option>
                        <option value="April ">April </option>
                        <option value="May">May</option>
                        <option value="June">June</option>
                        <option value="July ">July </option>
                        <option value="August">August</option>
                        <option value="September">September</option>
                        <option value="October">October</option>
                        <option value="November">November</option>
                        <option value="December">December</option>
                    </select>
                 </div>
                 <div class="form-group">
                    <label>Amount:<span class="text-danger">*</span></label>
                    <input type="text" class="form-control" id="feesamt"/>   
                  </div>
                
                 
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" id="feesRecord">Submit</button>
          <button type="button" class="btn btn-primary" data-dismiss="modal" id="feesmodalclose">Close</button>
        </div>
      </div>
      
    </div>
  </div> -->
 
<!--  ---------------delete modal class------------------>
  <div class="modal fade" id="delModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Delete student profile</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          
        </div>
        <div class="modal-body">
  
            <div class="card-body">
              <input type="hidden" name="id" class="form-control classfield" id="inputdelid">
              <p class="text-center">Do you want to delete?</p>
                 
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" id="delRecord">Delete</button>
          <button type="button" class="btn btn-primary" data-dismiss="modal" id="delmodalclose">Close</button>
        </div>
      </div>
      
    </div>
  </div>

<?php include("inc/footer.php");?>
<script>
  $(function () {
    $("#example1").DataTable({
      "responsive": false,
      "autoWidth": true,
      "info":false,
    });

  });
 
 // promotion modal data fetch
  function promotion(id)
  {
    var pid= id;
    
    $.ajax({
            url:"promotion.php",
            type:"get",
            data:{pid:pid},
            dataType:'json',
            success:function(data)
            {
                /*console.log(data);*/
                var result=data;
                $('#inputproid').val(result[0].all.id);
                $('#stdname').html(result[0].all.sname);
                $('#stdroll').val(result[0].all.roll);
              /*  $('#selclass').html(result[0].all.cname);*/
                $('#stdsec').val(result[0].all.section);
                
                $('#stdclass').html("");
                for(var i=0; i<result.length;i++)
                {
                   if(i==0)
                   {
                     $('#stdclass').append('<option disabled selected value="'+result[i].class.id+'">'+result[i].class.cname+'</option>') ; 
                   }else{
                    $('#stdclass').append('<option value="'+result[i].class.id+'">'+result[i].class.cname+'</option>');
                   }
                }
            },
        });
  };
  
 // promition modal action
 
  $(document).on('click','#probtn',function(){
     $('#proModal').modal();
     $(this).closest('tr').addClass('removeRow');
  });
 
  
  
// insert promoted details
        $(document).on('click','#proRecord',function(){
            
            var pid=$('#inputproid').val();
            var classname=$('#stdclass').val();
            var roll =$('#stdroll').val();
            var sec=$('#stdsec').val();
            
            $.ajax({
                url:"promotionins.php",
                type:"post",
                data:{pid:pid,
                    classname:classname,
                    roll:roll,
                    sec:sec
                },
                dataType:'json',
                success:function(data)
                {
                  /* console.log(data);*/
                   if(data.status==1){
    
                  $('#promodalclose').trigger('click');
                   $('.removeRow').animate({
                       opacity:0,
                   },"slow");
                   setTimeout(function(){
                    $('.removeRow').remove();   
                   },500),
                   
                   $('#alertmsg').slideDown();
                   $('#msg').html(data.msg);
                   $('#msg').css('color','green');
                       
                   }
                   else{
                    $('#pro_err').html(data.msg+" Of Roll: "+data.roll+" Section: "+data.sec);
                    $('#pro_err').css('color','red');
                   }
            
                },
            });
           
        });
        
        
//fees modal
/*$(document).on('click','#feesbtn',function(){
    $('#feesModal').modal();
});*/
        
 
 
// data fech for fees modal
  /*function payment(id)
  {
    var fid= id;*/
    /*console.log(fid);*/
 /*   $.ajax({
            url:"feesdata.php",
            type:"get",
            data:{fid:fid},
            dataType:'json',
            success:function(data)
            {*/
                /*console.log(data);*/
               /* var result=data;
                $('#feesid').val(result[0].id);
                $('#feesname').html(result[0].sname);
                $('#feesroll').html(result[0].roll);
                $('#feesclass').html(result[0].cname);
                $('#feessec').html(result[0].section);

            },
        });
  };
 */
//fees message close
/*   $(document).on('click','#msgid',function(){
        $("#alertmsg").slideUp();
    });
    */
//insert fees data

/*$(document).on('click','#feesRecord',function(){
    var sid=$('#feesid').val();
    console.log(sid);
    var sname=$('#feesname').html();
    console.log(sname);
    var sroll=$('#feesroll').html();
    var sclass=$('#feesclass').html();
    var ssec=$('#feessec').html();
    var amt=$('#feesamt').val();
    var month=$('#feesmonth').val();*/
  /*  console.log(month);*/
    
   /*   $.ajax({
                url:"feesdatainc.php",
                type:"post",
                data:{sid:sid,
                    sname:sname,
                    sroll:sroll,
                    sclass:sclass,
                    ssec:ssec,
                    amt:amt,
                    month:month,
                },
                dataType:'json',
                success:function(data)
                {
                   console.log(data);
                    $('#feesmodalclose').trigger('click');
                    $('#alertmsg').slideDown();
                    $('#msg').html(data.msg);
                    $('#msg').css('color','green');
                    $('.form-control').val("");

                }
      });
    
    
})
*/
       
// delete modal
  function delprofile(id)
  {
      var did= id;
     
      $('#inputdelid').val(did)
  };
  
  // remove row from table
  $(document).on('click','#delbtn',function(){
     $('#delModal').modal();
     $(this).closest('tr').addClass('removeRow');
  });
  
  //delete message close
   $(document).on('click','#msgid',function(){
        $("#alertmsg").slideUp();
    });
        
  // delete from dastabase
  $(document).on('click','#delRecord',function()
  {
      var did=$('#inputdelid').val();
      /* console.log(did);*/
      $.ajax({
          url:"delprofile.php",
          type:"post",
          data:{did:did},
          dataType:"json",
          success:function(data)
          {
          /*  console.log(data);*/
           
           $('#delmodalclose').trigger('click');
           $('.removeRow').animate({
               opacity:0,
           },"slow");
           setTimeout(function(){
            $('.removeRow').remove();   
           },500),
           
           $('#alertmsg').slideDown();
           $('#msg').html(data.msg);
           $('#msg').css('color','red');  
          }
      })
  });
  
</script>
</body>
</html>
