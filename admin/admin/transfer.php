<?php
include('mpdf/vendor/autoload.php');

include('inc/db.php');

$html='<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Tethibari Sarada Sishu Vidyamandir</title>
    <link rel="stylesheet" href="balancecss/style.css" media="all" />

  </head>
  <body>
    <header class="">
      <div style="text-align:center; padding-bottom:20px;">
        <div style="font-size:30px;font-family:SourceSeri;"><b>TETHIBARI SARADA SISHU VIDYAMANDIR</b></div>
        <div style="font-size:20px;">Estd. - 1998</div>
        <div style="font-size:20px; font-family:SourceSeri;"><i>Regd. No - S/IL/48426 of 2007-08</i></div>
        <div style="font-size:25px; font-family:SourceSeri;">Tethibari || Kismat Bajkul || Purba Medinipur</div>
        
      </div>
      <h1>Transfer Certificate</h1>

    </header>
    <main style="position:relative;">
    	<div class="main-form-start">';
    	
    	$sid=$_POST['sid'];// student id 
    	$paydate=$_POST['paydate'];
    	$c=trim($_POST['class_name']);
   
    	$reason=$_POST['reason'];
    	
    	$sel="SELECT * FROM studentinfo WHERE id=$sid";
    	$res=$con->query($sel);
    	$row=$res->fetch_assoc();
    	 $r=(strlen($row['roll'])>1)?$row['roll']:"0".$row['roll'];
    	 
    	 $html.='<div style="padding:20px 10px 0px 20px;">TC NO:'.substr($c,0,1).substr($c,-1).$r.$row['section'].'</div>
    	    <div class="demo " style="padding-top:30px;">
        	     <div class="demo-txt-bold" style="width:20%; font-family:amaranth;">Certified that</div>
        	     <div class="brder-txt" style="width:80%;  text-transform: uppercase; font-family:amaranth;" ><span><b>'.$row['sname'].'</b></div>
    	     </div>
    	     <div class="demo">
        	     <div class="demo-txt-bold" style="width:30%; font-family:amaranth;">Son/ Daughter of</div>
        	     <div class="brder-txt" style="width:70%; text-transform: uppercase; font-family:amaranth;"><span><b> '.$row['gname'].'</b></div>
    	     </div>
    	     <div class="demo">
        	     <div class="demo-txt-bold" style="width:30%; font-family:amaranth;">an inhabitant of</div>
        	     <div class="brder-txt" style="width:70%; text-transform: uppercase; font-family:amaranth;"><span><b>'.$row['address'].'</b></div>
    	     </div>
    	     <div class="demo">
        	     <div class="demo-txt-bold" style="width:20%; font-family:amaranth;">Post Office</div>
        	     <div class="brder-txt" style="width:80%; text-transform: uppercase; font-family:amaranth;"><b>'.$row['postoffice'].'</b></div>
    	     </div>
    	     <div class="demo">
        	     <div class="demo-txt-bold" style="width:20%; font-family:amaranth;">Police station</div>
        	     <div class="brder-txt" style="width:80%; text-transform: uppercase; font-family:amaranth;"><b>'.$row['village'].'</b></div>
    	     </div>
    	     <div class="demo">
        	     <div class="demo-txt-bold" style="width:30%; font-family:amaranth;">In the District of</div>
        	     <div class="brder-txt" style="width:40%; text-transform: uppercase;  font-family:amaranth;"><b>'.$row['policesta'].'</b></div>
        	     <div class="demo-txt-bold" style="width:20%; font-family:amaranth;">left Thethibari</div>
    	     </div>
    	     <div class="demo">
        	     <div class="demo-txt-bold" style="width:40%; font-family:amaranth;">Sarada Sishu Vidyamandir on</div>
        	     <div class="brder-txt" style="width:60%; float:left ; font-family:amaranth;"><b>'.date('d-m-Y').'</b></div>
    	     </div>
    	     <div class="demo">
    	     <div class="demo-txt-bold" style="width:80% ; font-family:amaranth;"> His/Her date of birth is (according to the Admission Registered)</div>
        	     <div class="brder-txt" style="width:20%; float:left; font-family:amaranth;"><b>'.date('d-m-Y',strtotime($row['cdate'])).'</b></div>
    	     </div>';
    	     $cid=$row['class'];//get class id 
    	     $selc="SELECT * FROM addclass WHERE id=$cid"; 
    	     $resc=$con->query($selc);
    	     $rowc=$resc->fetch_assoc();
    	     $html.='<div class="demo">
        	     <div class="demo-txt-bold" style="width:45%;font-family:amaranth;">He/She is/was reading in class</div>
        	     <div class="brder-txt " style="width:20%; font-family:amaranth;"><b>'.$rowc['cname'].'</b></div>
        	     <div class="demo-txt-bold" style="width:25%; font-family:amaranth;">and had/had not </div>
    	     </div>
    	     <div class="demo">
        	     <div class="demo-txt-bold" style="width:80%; font-family:amaranth;">passed the Annual Examination for promotion to the class</div>
        	     <div class="brder-txt" style="width:20%; float:left; font-family:amaranth;"><b>'.$c.'</b></div>
    	     </div>
    	      <div class="demo">
        	     <div class="demo-txt-bold" style="width:82%; font-family:amaranth;">All sums due by him/her have been paid viz Fees and Fines upto</div>
        	     <div class="brder-txt" style="width:18%; float:left; font-family:amaranth;"><b>'.$paydate.'</b></div>
    	     </div>
    	     <div class="demo" style="text-align:center;">
        	     <div class="demo-txt-bold" style="width:40%; text-align: right; font-family:amaranth;"> Character :</div>
        	     <div class="brder-txt" style="width:60%; float:left; font-family:amaranth;"><b>GOOD</b></div>
    	     </div>
    	     <div id="" style="width:50%; float:left; text-align:center; padding: 20px 0px 40px 0px; font-family:amaranth; font-size:16px;">
              Reason For Leaving 
            </div>';
            if($reason == 1){
              $html.='<div style="width:50%; text-align:center; padding: 20px 0px 40px 0px; font-family:amaranth; font-size:16px;">
            <div><b>Completion of the school</b></div>
            
           </div>';  
            }else  if($reason == 2){
              $html.='<div style="width:50%; text-align:center; padding: 20px 0px 40px 0px; font-family:amaranth; font-size:16px;">
            <div><b>Unavailable chenge of residence</b></div>
            
           </div>';  
            }else  if($reason == 3){
              $html.='<div style="width:50%; text-align:center; padding: 20px 0px 40px 0px; font-family:amaranth; font-size:16px;">
            <div><b>Abolition or closure of school</b></div>
            
           </div>';  
            }else{
              $html.='<div style="width:50%; text-align:center; padding: 20px 0px 40px 0px; font-family:amaranth; font-size:16px;">
            <div><b>Minor or private reasons other than the foregoing</b></div>
            
           </div>';  
            }
        
         $html.='</div>
      <div id="" style="width:20%; float:left; text-align:center; padding: 20px 0px 40px 0px;  font-size:14px;">
              Dated:
            </div>
    
          <div style="width:40%; text-align:center; padding: 20px 0px 40px 0px;  font-size:14px;float:left;">
            <div> Head Mistress</div>
            
           </div> 
           <div style="width:40%; text-align:center; padding: 20px 0px 40px 0px;  font-size:14px;">
            <div>Secretary</div>
            
           </div>
           
    </main>
 
  </body>
</html>';

$mpdf=new \Mpdf\Mpdf();

$css=file_get_contents('balancecss/style.css');
$mpdf->SetWatermarkImage('image/tssv-watermark.png');
$mpdf->showWatermarkImage  = true;
$mpdf->WriteHTML($css,1);
$mpdf->WriteHTML($html);

$mpdf->output('Tensfer_Certificate'.date("m-d-Y_H_i_s").'.pdf','D');
?>