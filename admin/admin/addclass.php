<?php
session_start();
if(!isset($_SESSION['uid']))
{
    header("location:index.php");
}
?>
<!DOCTYPE html>
<html>
<?php include("inc/header.php");?>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  <?php include("inc/navbar.php");
        include("inc/sidebar.php")
  ?>

  

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Add Classes</h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
      <div class="row">
        <div class="col-md-5">

            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Add Class Form</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" id="quickForm" method="post">
                <div class="card-body">
                  <div class="form-group">
                    <label>Enter Class</label>
                    <input type="text" name="class" class="form-control classfield" id="inputclass">
                     <span id="errmsg1"></span>
                  </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="button" class="btn btn-primary" id="addRecord">Submit</button>
                </div>
              </form>
            <div class="alert alert-default" style="display:none;" id="addalertmsg">
                <span class="addmsgclose float-right" id="addmsgid" style="cursor:pointer;"><i class="fas fa-times"></i></span>
                <strong id="addmsg"></strong>
            </div>
            </div> 
        <h4 id="msg"></h4>
        </div>  
       
        <div class="col-md-7">
            <div class="alert alert-default" style="display:none;" id="alertactionmsg">
                <span class="actionmsgclose float-right" id="actionmsgid" style="cursor:pointer;"><i class="fas fa-times"></i></span>
                <strong id="actionmsg"></strong>
            </div>
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Classes List</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example2" class="table table-bordered table-hover">
                  <thead>
                  <tr>
                    <th>Classes</th>
                    <th style="width: 180px" colspan="2" >Actions</th>
                  </tr>
                  </thead>
                  <tbody id="tdody_id">
                 
                  </tbody>

                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

            </div> 
        </div>
      </div>
        
      
      </div>
    </section>
    <!-- /.content -->
  </div>
<!--  ---------------edit modal class------------------>
  <div class="modal fade" id="editModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Edit classes</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          
        </div>
        <div class="modal-body">
  
            <div class="card-body">
              <div class="form-group">
                <input type="hidden" name="id" class="form-control classfield" id="inputeditid">
                <label>Enter Class</label>
                <input type="text" name="class" class="form-control classfield" id="inputeditclass">
                <span id="errmsg2"></span>
              </div>
                 
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" id="editRecord">Update</button>
          <button type="button" class="btn btn-primary" data-dismiss="modal" id="editmodalclose">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  
  <!--  ---------------delete modal class------------------>
  <div class="modal fade" id="delModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Delete classes</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          
        </div>
        <div class="modal-body">
  
            <div class="card-body">
              <input type="hidden" name="id" class="form-control classfield" id="inputdelid">
              <p class="text-center">Do you want to delete?</p>
                 
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" id="delRecord">Delete</button>
          <button type="button" class="btn btn-primary" data-dismiss="modal" id="delmodalclose">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  
  
</div>

<?php include("inc/footer.php");?>
<script>
  $(function () {

    $('#example2').DataTable({
      "paging": false,
      "lengthChange": false,
      "searching": false,
      "ordering": false,
      "info": false,
      "autoWidth": false,
      "responsive": true,
    });
  });
</script>

 <script>
 

        $(document).ready(function(){

            readRecord(); 

         });

         
        function readRecord()
        {
             $.ajax({
                 url:'fetchclass.php',
                 type:'post',
                 dataType:'json',
                 success:function(data)
                 {
                     /*console.log(data);*/
                     $('.classfield').val("");
                     $('#tdody_id').html("");
                     var result=data;
                     for(var i=0; i<result.length; i++){
                         //alert(result[i]);
                         $('#tdody_id').append('<tr><td>'+result[i].cname+'</td><td><a type="button" data-toggle="tooltip" title="Edit" onclick="editclass('+result[i].id+')" id="editBtn" class="updbtn text-primary"><i class="fas fa-edit" aria-hidden="true"></i></a></td><td><a type="button" onclick="deleteclass('+result[i].id+')" data-toggle="tooltip" title="Delete"  id="delbtn" class="delbtn text-danger" ><i class="far fa-trash-alt" aria-hidden="true"></i></a></td></tr>');
                       }
                 }   
                 
             });
        }
        
/*---------------add class_message----------------*/

    $(document).on('click','#addmsgid',function()
    {
      $('#addalertmsg').slideUp();  
    });
        
/*    ----------------add data--------------------  */
        $(document).on('click','#addRecord',function(){
             
           var class_name=$('#inputclass').val();
          /* console.log(class_name);*/
         /*  var section=$('#inputsection').val();
           console.log(section);*/
           if(class_name=="")
           {
               $('#errmsg1').html('** Class name must be filled');
               $('#errmsg1').css('color','red');
               return false;
           }/*
           if(section=="")
           {
               $('#errmsg2').html('**Section must be filled');
               $('#errmsg2').css('color','red');
               return false;
           }*/
           
           
           $.ajax({
               url:"classins.php",
               type:"post",
               data:{cname:class_name},
               dataType:'json',
               success:function(data){
                 /*  console.log(data)*/
               },
              complete:function(data)
             {
                 $('#addalertmsg').slideDown();
                 $('#addmsg').html(data.responseJSON.msg);
                 $('#addmsg').css('color','green');
                 $('.classfield').val("");
                 
                 readRecord();

                 
             }
               
           });
        }) ;
        
/*----------------editclass-----------------   */     
        function editclass($id)
        {
            
            var eid=$id;
            $.ajax({
                url:"editclass.php",
                type:"get",
                data:{eid:eid},
                dataType:'json',
                success:function(data)
                {
                    var result=data;
                    $('#inputeditid').val(result[0].id);
                    $('#inputeditclass').val(result[0].cname);
                },
            });
        }
        
       /*    ------------edit modal--------------*/
        $(document).on('click','#editBtn',function(){
            $("#editModal").modal();
        });
        
    /*        ------------------editclass insertion---------------*/
        $(document).on('click','#editRecord',function(){
        
           var eid=$('#inputeditid').val();
           /*console.log(eid);*/
           var edit_class=$('#inputeditclass').val();
        
          if(edit_class=="")
           {
               
               $('#errmsg2').html('** Class name must be filled');
               $('#errmsg2').css('color','red');
               return false;
           }
           $.ajax({
               url:"editclassins.php",
               type:"POST",
               data:{
                 eid:eid,
                 edit_class:edit_class},
               dataType:'json',
               success:function(data){
                   /*console.log(data);*/
                   $('#editmodalclose').trigger('click');
                   $('#alertactionmsg').slideDown();
                   $('#actionmsg').html(data.msg);
                   readRecord();
               }

             });
           
        });
     $(document).on('click','#actionmsgid',function(){
            $("#alertactionmsg").slideUp();
        });
        
  /*    ------------delete modal--------------*/
 
    $(document).on('click','#delbtn',function(){
        $('#delModal').modal();
    })
 
 
  /*------------------delete id---------------*/
        function deleteclass($id)
        {
            var did=$id;

         /*   console.log(did);*/
            $("#inputdelid").val(did);

           
        }
          /*------------------delete table---------------*/
        $(document).on('click','#delRecord',function(){
            var did=$('#inputdelid').val();
            $.ajax({
                url:"delclass.php",
                type:"get",
                data:{did:did},
                dataType:'json',
                success:function(data)
                {
                  /* console.log(data);*/
                   $('#delmodalclose').trigger('click');
                   $('#alertactionmsg').slideDown();
                   $('#actionmsg').html(data.msg);
                   readRecord();
                },
            });
           
        });
        
     
 </script>
</body>
</html>
