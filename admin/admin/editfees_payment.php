<?php

include("inc/db.php");
session_start();
$fmonth="";
if(!isset($_SESSION['uid']))
{
    header("location:index.php");
}
?>
<!DOCTYPE html>
<html>
  <?php include("inc/header.php");?>
 


<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  <?php include("inc/navbar.php");
        include("inc/sidebar.php")
  ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
       <!--     <h1 class="m-0 text-dark">Student Details Form</h1>-->
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
            <div class="card card-primary">
              <div class="card-header">
                <h1 class="card-title">Fees Payment Form</h1>
                
             </div>
            <?php
            
            $id=$_GET['id'];// id of fees_table
            $sel="SELECT studentinfo.*, fees_table.* FROM studentinfo RIGHT JOIN fees_table ON studentinfo.id = fees_table.sid WHERE fees_table.id=$id ";
            $rs=$con->query($sel);
            $row=$rs->fetch_assoc();
            $len=strlen($row['month']);
            $month=explode(" ",$row['month']);
            
            if(count($month)==2){
                $fmonth=$month[0];
            }
            if(count($month)==4){
                $fmonth1=$month[0];
                $fmonth2=$month[2];
            }
            ?>
             <input type="hidden" id="feesid" value="<?php echo $row['id'];?>"/>
             <input type="hidden" id="sid" value="<?php echo $row['sid'];?>"/>
             <div class="card-body" id="feeForm">
                 <div id="selectMon">
                     <label>Select Payment Type:<span class="text-danger">*</span></label><br>
                     <input type="radio" name="fee-month" class="feemonth" id="single" <?php if($row['feetype']=='single'){ echo "checked";}?> value="single"> <b>Single Month </b><br>
                     <input type="radio" name="fee-month" class="feemonth" id="multi" <?php if($row['feetype']=='multi'){ echo "checked";}?> value="multi"> <b>Multiple Month </b> 
                 </div>
                
                 <div  style=" padding-top:30px;">
                    <form>
                        
                        <div class="row">
                            <div class="col-md-8" style="padding-top:30px;">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                          <label>Student Name:</label> <span id="feesname"><?php echo $row['sname'];?></span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                       <div class="form-group">
                                           <label>Roll:</label> <span id="feesroll"><?php echo $row['roll'];?></span>
                                        </div> 
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <?php
                                            $cid=$row['class'];// class id for fetch class name
                                            $selc="SELECT * FROM addclass WHERE id=$cid";
                                            $rsc=$con->query($selc);
                                            $rowc=$rsc->fetch_assoc();
                                            ?>
                                          <label>Class:</label> <span id="feesclass"><?php echo $rowc['cname'];?></span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                       <div class="form-group">
                                          <label>Section:</label> <span id="feessec"><?php echo $row['section'];?></span>
                                        </div>
                                    </div>
                                </div>
                            </div> 
                            <div class="col-md-4">
                                <?php if($row['image']==""){
                                        if($row['gender']=='Male'){
                                        ?>
                                         <img style="border:1px solid #ccc; padding:10px; width:150px;" src="../admin/image/profile-male.png">
                                        <?php
                                        }else{
                                        ?>
                                        <img style="border:1px solid #ccc; padding:10px; width:150px;" src="../admin/image/profile-female.png">
                                        <?php
                                        }
                                    }else{
                                    ?>
                                    <img style="border:1px solid #ccc; padding:10px; width:150px;" src="../admin/studentimg/<?php echo $row['image'];?>">
                                    <?php
                                    }
                                ?>
                                
                            </div>
                         </div>
                        <div class="row">
                           
                            <div class="col-md-4" style="display:none;" id="single_div">
                               <div class="form-group monthdiv">
                                  
                                    <label>Month of:<span class="text-danger">*</span></label>
                                    <select class="form-control" id="single_month">
                                        <option value="" disabled selected>Select Month</option>
                                        <option  <?php if($fmonth=='January'){ echo "selected";}?> value="January">January</option>
                                        <option <?php if($fmonth=='February'){ echo "selected";}?> value="February">February</option>
                                        <option value="March">March</option>
                                        <option value="April ">April </option>
                                        <option value="May">May</option>
                                        <option value="June">June</option>
                                        <option value="July ">July </option>
                                        <option value="August">August</option>
                                        <option value="September">September</option>
                                        <option value="October">October</option>
                                        <option value="November">November</option>
                                        <option value="December">December</option>
                                    </select>
                               </div> 
                            </div>
                            <div class="col-md-4"  style="display:none;" id="multi_div">
                                <div class="form-group monthdiv"  >
                                    <label>Month Of:<span class="text-danger">*</span></label>
                                    <div class="row">
                                        <div class="col-md-6" id="monthdiv1">
                                         <select class="form-control" id="multi_month1">
                                            <option value="" disabled selected>From</option>
                                            <option <?= ($fmonth1=="Jan")?"selected":"" ?> value="Jan">January</option>
                                            <option <?= ($fmonth1=="Feb")?"selected":"" ?> value="Feb">February</option>
                                            <option <?= ($fmonth1=="Mar")?"selected":"" ?> value="Mar">March</option>
                                            <option <?= ($fmonth1=="Apr")?"selected":"" ?> value="Apr">April </option>
                                            <option <?= ($fmonth1=="May")?"selected":"" ?> value="May">May</option>
                                            <option <?= ($fmonth1=="Jun")?"selected":"" ?> value="Jun">June</option>
                                            <option <?= ($fmonth1=="Jul")?"selected":"" ?> value="Jul">July </option>
                                            <option <?= ($fmonth1=="Aug")?"selected":"" ?> value="Aug">August</option>
                                            <option <?= ($fmonth1=="Sept")?"selected":"" ?> value="Sept">September</option>
                                            <option <?= ($fmonth1=="Oct")?"selected":"" ?> value="Oct">October</option>
                                            <option <?= ($fmonth1=="Nov")?"selected":"" ?> value="Nov">November</option>
                                            <option <?= ($fmonth1=="Dec")?"selected":"" ?> value="Dec">December</option>
                                        </select> 
                                        </div>
                                        <div class="col-md-6" id="monthdiv2">
                                          <select class="form-control" id="multi_month2">
                                            <option value="" disabled selected>To</option>
                                            <option <?= ($fmonth2=="Jan")?"selected":"" ?> value="Jan">January</option>
                                            <option <?= ($fmonth2=="Feb")?"selected":"" ?> value="Feb">February</option>
                                            <option <?= ($fmonth2=="Mar")?"selected":"" ?> value="Mar">March</option>
                                            <option <?= ($fmonth2=="Apr")?"selected":"" ?> value="Apr">April </option>
                                            <option <?= ($fmonth2=="May")?"selected":"" ?> value="May">May</option>
                                            <option <?= ($fmonth2=="Jun")?"selected":"" ?> value="Jun">June</option>
                                            <option <?= ($fmonth2=="Jul")?"selected":"" ?> value="Jul">July </option>
                                            <option <?= ($fmonth2=="Aug")?"selected":"" ?> value="Aug">August</option>
                                            <option <?= ($fmonth2=="Sept")?"selected":"" ?> value="Sept">September</option>
                                            <option <?= ($fmonth2=="Oct")?"selected":"" ?> value="Oct">October</option>
                                            <option <?= ($fmonth2=="Nov")?"selected":"" ?> value="Nov">November</option>
                                            <option <?= ($fmonth2=="Dec")?"selected":"" ?> value="Dec">December</option>
                                        </select>  
                                        </div>
                                    </div>
                               </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group" id="namediv">
                                    <label>Tuition Fee:<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control"  id="tfee" value="<?php echo $row['tfee'];?>" />
                               </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group" id="namediv">
                                    <label>Development Fee:</label>
                                    <input type="text" class="form-control"  id="dfee" value="<?php echo $row['dfee'];?>" />
                               </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                             <div class="form-group" id="namediv">
                                    <label>Addmission/Transfer:</label>
                                    <input type="text" class="form-control"  id="add" value="<?php echo $row['add'];?>" />
                               </div> 
                            </div>
                            <div class="col-md-4">
                                <div class="form-group" id="namediv">
                                    <label>Games & Sports:</label>
                                    <input type="text" class="form-control"  id="game" value="<?php echo $row['game'];?>" />
                               </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group" id="namediv">
                                    <label>Social Fee:</label>
                                    <input type="text" class="form-control"  id="sfee" value="<?php echo $row['sfee'];?>" />
                               </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                             <div class="form-group" id="namediv">
                                    <label>Magazine Fee:</label>
                                    <input type="text" class="form-control"  id="mfee" value="<?php echo $row['mfee'];?>" />
                               </div> 
                            </div>
                            <div class="col-md-3">
                                <div class="form-group" id="namediv">
                                    <label>Electricity:</label>
                                    <input type="text" class="form-control"  id="ele" value="<?php echo $row['ele'];?>"/>
                               </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group" id="namediv">
                                    <label>Establishment Fee:</label>
                                    <input type="text" class="form-control"  id="est" value="<?php echo $row['est'];?>"/>
                               </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group" id="namediv">
                                    <label>Computer Fee:</label>
                                    <input type="text" class="form-control"  id="cfee" value="<?php echo $row['cfee'];?>"/>
                               </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                             <div class="form-group" id="namediv">
                                    <label>Exam Fee:</label>
                                    <input type="text" class="form-control"  id="efee" value="<?php echo $row['efee'];?>" />
                               </div> 
                            </div>
                            <div class="col-md-3">
                                <div class="form-group" id="namediv">
                                    <label>Tour/Picnic:</label>
                                    <input type="text" class="form-control"  id="tour" value="<?php echo $row['tour'];?>"/>
                               </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group" id="namediv">
                                    <label>School Van Fee:</label>
                                    <input type="text" class="form-control"  id="school" value="<?php echo $row['vfee'];?>"/>
                               </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group" id="namediv">
                                    <label>Other Charges:</label>
                                    <input type="text" class="form-control"  id="other" value="<?php echo $row['other'];?>" />
                               </div>
                            </div>
                        </div>
                        
                       <input type="button" class="btn btn-primary" id="sub" value="Submit">
                       <a href="feesdetails.php?id=<?php echo $row['class'];?>" class="btn btn-primary">Back--></a>
                      
                    </form> 
                 </div>
             </div>
            </div>
      
      </div>
    <div class="alert alert-default" style="display:none;" id="alertmsg">
        <span class="msgclose float-right" id="msgid" style="cursor:pointer;"><i class="fas fa-times"></i></span>
        <strong id="msg"></strong>
        <a href="fees-receipt2.php?id=<?php echo $sid;?>" class="btn btn-link" style="text-align:center; color: #007bff;"><i class="fas fa-download"></i></a>
    </div>
    </section>
    <!-- /.content -->
  </div>
  
<!-------------style for form validation---------------------->
<style>

.error_form {
    border: 2px solid red;
}
.error {
  color: red;
  margin-left: 5px;
}

label.error {
  display: inline;
  
}
</style>


<?php include("inc/footer.php");?>


<script>

$(document).ready(function(){
    
    
    var feetype=$(".feemonth:checked").val();
    console.log(feetype);
    if(feetype=="single"){
        $('#single_div').slideDown();
    
    }
    else{
      
        $('#multi_div').slideDown();
    }
/*    if(document.getElementById(box_id).checked==true){
        if(month=='single'){
            $('#single_div').slideDown();
            $(".error").remove();
            $("#feeForm").removeClass('error_form');
            $('#multi_div').slideUp();
        }else{
           $('#single_div').slideUp();
           $(".error").remove();
           $("#feeForm").removeClass('error_form');
           $('#multi_div').slideDown(); 
       }*/
})

$(document).on('click','.feemonth',function(){
    
    var box_id=$(this).attr('id');
    var month=$(this).val()
    if(document.getElementById(box_id).checked==true){
        if(month=='single'){
            $('#single_div').slideDown();
            $(".error").remove();
            $("#feeForm").removeClass('error_form');
            $('#multi_div').slideUp();
        }else{
           $('#single_div').slideUp();
           $(".error").remove();
           $("#feeForm").removeClass('error_form');
           $('#multi_div').slideDown(); 
        }
        
    }
    
    
});

$(document).on('click','#sub',function(){
   
    var fid=$('#feesid').val();
    var sid=$('#sid').val();
    var sname=$('#feesname').html();
    var sroll=$('#feesroll').html();
    var sclass=$('#feesclass').html();
    var ssec=$('#feessec').html();
    var feemonth=$('.feemonth:checked').val();
    var paymonth=(feemonth=='single')? $('#single_month').val():$('#multi_month1').val()+" To "+$('#multi_month2').val();
    var tfee=$('#tfee').val();
    var dfee=$('#dfee').val();
    var add=$('#add').val();
    var game=$('#game').val();
    var sfee=$('#sfee').val();
    var mfee=$('#mfee').val();
    var ele=$('#ele').val();
    var est=$('#est').val();
    var cfee=$('#cfee').val();
    var efee=$('#efee').val();
    var tour=$('#tour').val();
    var school=$('#school').val();
    var other=$('#other').val();
    
     // form validation
            
        $(".error").remove();
        $("#feeForm").removeClass('error_form');
        
        if($('input[type=radio][name=fee-month]:checked').length == 0){
            $("#selectMon").after('<span class="error">This field is required</span>');
            $('#feeForm').addClass('error_form');
            $('html,body').animate({
              scrollTop: $("#selectMon").offset().top
            }, 500);
          return false;
        }
        if(paymonth == null || paymonth== 'null To null'){
            $(".monthdiv").after('<span class="error">This field is required</span>');
            $('#feeForm').addClass('error_form');
            $('html,body').animate({
              scrollTop: $("#selectMon").offset().top
            }, 500);
          return false;
        }
        
        if(tfee.length < 1){
            $("#tfee").after('<span class="error">This field is required</span>');
            $('#feeForm').addClass('error_form');
            $('html,body').animate({
              scrollTop: $("#selectMon").offset().top
            }, 500);
          return false;
        }
        
    
    $.ajax({
        
        url:"editfee_paymentinc.php",
        type:"post",
        data:{
            fid:fid,
            sid:sid,
            sname:sname,
            sroll:sroll,
            sclass:sclass,
            ssec:ssec,
            feetype:feemonth,
            paymonth:paymonth,
            tfee:tfee,
            dfee:dfee,
            add:add,
            game:game,
            sfee:sfee,
            mfee:mfee,
            ele:ele,
            est:est,
            cfee:cfee,
            efee:efee,
            tour:tour,
            vfee:school,
            other:other
        },
        dataType:'json',
        success:function(data){
            
                $('#alertmsg').slideDown();
                $('#msg').html(data.msg);
                $('#msg').css('color','green');
                $('.form-control').val("");

        }
    })
    
});
//fees message close
   $(document).on('click','#msgid',function(){
        $("#alertmsg").slideUp();
    });
    
</script>
</body>
</html>
