<?php

include("inc/db.php");
session_start();
if(!isset($_SESSION['uid']))
{
    header("location:index.php");
}
?>
<!DOCTYPE html>
<html>
<?php include("inc/header.php");?>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  <?php include("inc/navbar.php");
        include("inc/sidebar.php")
  ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">



    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <?php
                    $id=$_GET['id'];
                    $sel="SELECT * FROM studentinfo where id='$id'";
                    $rs=$con->query($sel);
                    $row=$rs->fetch_assoc();
                    ?>

            <div class="card">
              <div class="card-header">
                <h3 class="card-title"><?php echo $row['sname'];?></h3>
                   <a href="viewstudent.php?id=<?php echo $row['class'];?>" class="btn btn-primary float-right text-white">Back <i class="fas fa-arrow-circle-right"></i></a>
                <a href="viewstudentsingle_pdf.php?id=<?php echo $row['id'];?>" class="btn btn-primary float-right" style="margin-right: 5px;">
                    <i class="fas fa-download"></i> Admission Form
                </a>
             
           <!--     <a href="viewstudentsingle-print.php?id=<?php echo $row['id'];?>" target="_blank" class="btn btn-default"><i class="fas fa-print"></i> Print</a>-->
              </div>
              <!-- /.card-header -->
              <div class="card-body table-responsive">
                    <div class="image-container text-center" >
                                <?php 
                        $image=$row['image'];
                        if($image=="")
                        {
                              if($row['gender'] == 'Male'){
                            ?>
                             <img style="border:1px solid #ccc; padding:10px; width:150px;" src="../admin/image/profile-male.png">
                             <?php
                             }else{
                             ?>
                             <img style="border:1px solid #ccc; padding:10px; width:150px;" src="../admin/image/profile-female.png">
                            <?php
                             }   
                        } else{
                        ?>
                        <img style="border:1px solid #ccc; padding:10px; width:150px;" src="../admin/studentimg/<?php echo $row['image'];?>">
                        <?php
                        }
                        ?></div><br>
                    <h5 class="text-center">Personal Information:</h5>
                    <table class="table table-bordered table-striped">
                        <tr>
                            <td class="pr-4"><b>Name</b></td>
                            <td><?php echo $row['sname'];?></td>
                        </tr>
                        <?php
                            $cid=$row['class'];
                            $selc="SELECT * FROM addclass WHERE id='$cid'";
                            $rsc=$con->query($selc);
                            $rowc=$rsc->fetch_assoc();
                        ?>
                        <tr>
                            <td class="pr-4"><b>Class</b></td>
                            <td><?php echo $rowc['cname'];?></td>
                        </tr>
                        <tr>
                            <td class="pr-4"><b>Roll Number</b></td>
                            <td><?php echo $row['roll'];?></td>
                        </tr>
                        <tr>
                            <td class="pr-4"><b>Section</b></td>
                            <td><?php echo $row['section'];?></td>
                        </tr>
                        <tr>
                            <td class="pr-4"><b>Gender</b></td>
                            <td><?php echo $row['gender'];?></td>
                        </tr>
                        <tr>
                            <td class="pr-4"><b>DOB(DD/MM/YYYY)</b></td>
                            <td><?php echo $row['cdate'];?></td>
                        </tr>
                        <tr>
                            <td class="pr-4"><b>Blood Group</b></td>
                            <td><?php echo $row['blood'];?></td>
                        </tr>
                        <tr>
                            <td class="pr-4"><b>Guardian Name</b></td>
                            <td><?php echo $row['gname'];?></td>
                        </tr>
                        <tr>
                            <td class="pr-4"><b>Address</b></td>
                            <td><?php echo $row['address'];?></td>
                        </tr>
                        <tr>
                            <td class="pr-4"><b>Post Office</b></td>
                            <td><?php echo $row['postoffice'];?></td>
                        </tr>
                        <tr>
                            <td class="pr-4"><b>Village</b></td>
                            <td><?php echo $row['village'];?></td>
                        </tr>
                        <tr>
                            <td class="pr-4"><b>Police Station</b></td>
                            <td><?php echo $row['policesta'];?></td>
                        </tr>
                        <tr>
                            <td class="pr-4"><b>District</b></td>
                            <td><?php echo $row['dist'];?></td>
                        </tr>
                        <tr>
                            <td class="pr-4"><b>Pin Code</b></td>
                            <td><?php echo $row['pincode'];?></td>
                        </tr>
                        <?php if($row['aadhar']){?>
                        <tr>
                            <td class="pr-4"><b>Aadhar No</b></td>
                            <td><?php echo $row['aadhar'];?></td>
                        </tr>
                        <?php }?>
                        <tr>
                            <td class="pr-4"><b>Mobile Number</b></td>
                            <td><?php echo $row['phone1'];?></td>
                        </tr>
                        <?php if($row['phone2']){ ?>
                        <tr>
                            <td class="pr-4"><b>Mobile Number (Optional)</b></td>
                            <td><?php echo $row['phone2'];?></td>
                        </tr>
                            <?php } if($row['email']){ ?>
                        <tr>
                            <td class="pr-4"><b>Email</b></td>
                            <td><?php echo $row['email'];?></td>
                        </tr>
                        <?php }?>
                    </table>
                    
                    <br>
                    
                    <h5 class="text-center" style="padding-top:30px;">Fees Related Information:</h5>
                    <table class="table table-bordered table-striped">
                        <thead>
                          <tr>
                              <th>Payment For Month</th>
                              <th>Amount</th>
                              <th>Fees receipt</th>
                          </tr>  
                        </thead>
                        <tbody>
                           <?php
                                $fid=$_GET['id'];// fees  table id
                                $self="SELECT * FROM fees_table where sid='$fid'";
                                $rsf=$con->query($self);
                                while($rowf=$rsf->fetch_assoc()){
                              ?>
                             <tr>
                                <td class="text-capitalize feemonth" ><span class="feemonthname"><?php echo $rowf['month']; ?></span></td>
                                <td><?php echo $rowf['total'];?></td>
                                <td><button type="button" class="btn btn-link feedownbtn" style="text-align:center;" ><span id="feeid" class="d-none"><?php echo $fid;?></span><i class="fas fa-download"></i></button></td>
                            </tr>
                            <?php
                                }
                            ?>
                        </tbody>
                    </table>
                   <!-- -------------------transfer certificate form----------------------------->
                    <div style=" padding-top:30px;">
                        <h5 class="text-center pt-3">Transfer Certificate:</h5>
                             <form role="form" id="trnsForm" method="post" action="transfer.php">
                                 <input type="hidden" name="sid" value="<?php echo $row['id'];?>">
                                <div class="card-body">
                                <div class="row">
                                    <div class="col-md-6">
                                       <div class="form-group" >
                                        <label>Promoted Class:</label>
                                        <select name="class_name" class="form-control" id="stdclass" required>
                                            <option></option>
                                            <?php
                                             $last_id="SELECT * FROM addclass ORDER BY id DESC LIMIT 1";
                                             $res_last=$con->query($last_id);
                                             $row_last=mysqli_fetch_array($res_last);
                                             if($cid==$row_last['id']){
                                                ?>
                                                <option value="<?php echo $row_last['cname'];?>"><?php echo $row_last['cname'];?></option>
                                                <?php
                                                 
                                             }else{
                                             $sel_pc="SELECT * FROM addclass WHERE id>$cid";
                                             $res_pc=$con->query($sel_pc);
                                             while($row_pc=$res_pc->fetch_assoc()){
                                            ?>
                                            <option value="<?php echo $row_pc['cname'];?>"><?php echo $row_pc['cname'];?></option>
                                            <?php
                                             }
                                             }
                                            ?>
                                        </select>
                                      </div> 
                                    </div>
                                    <div class="col-md-6">
                                       <div class="form-group" >
                                          <label>All sum paid upto:<span class="text-danger">*</span></label>
                                            <div class="input-group date" id="reservationdate" data-target-input="nearest">
                                                <input type="text" name="paydate" class="form-control datetimepicker-input" data-target="#reservationdate" id="stddate"/>
                                                <div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
                                                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                                </div>
                                            </div>
                                        </div> 
                                    </div>
                                </div>
                                  
                                 
                                  
                                  <div class="form-group">
                                    <label>Reason For Leaving:</label>
                                    <select class="form-control" name="reason" id="reason">
                                        <option></option>
                                        <option value="1">Completion of the school</option>
                                        <option value="2">Unavailable chenge of residence</option>
                                        <option value="3">Abolition or closure of school</option>
                                        <option value="4">Minor or private reasons other than the foregoing</option>
                                        
                                    </select>
                                  </div>
                                </div>
                                
                       </form>
                       <!-- /.card-body -->
                                <div class="card-footer">
                                  <button type="submit" class="btn btn-primary" id="transbtn"><i class="fas fa-download"></i> Transfer Certificate</button>
                                </div>
                    </div>
                       <!-- /.hidden form for pdf -->
                    <form id="pdf_form" action="fees-receipt.php" method="post">
                         <input type="hidden" name="feeid" id="form_id">
                         <input type="hidden" name="feemonth" id="form_month"> 
                    </form>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<style>
    .error {
      color: red;
      margin-left: 5px;
    }
    
    label.error {
      display: inline;
      
    }
</style>
<?php include("inc/footer.php");?>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>


<script>

  $(function () {
    $("#example1").DataTable({
      "responsive": false,
      "autoWidth": true,
    });

  });
  /*---------------datepicker--------------*/
    $('#reservationdate').datetimepicker({
        format: 'DD/MM/YYYY'
    });
  $(document).on('click','#transbtn',function(){
      
        var sclass=$("#stdclass").val();
        var reason=$("#reason").val();
        var stddate=$("#stddate").val();
        
        $(".error").remove();
        if (sclass.length < 1) {
        $('#stdclass').after('<span class="error">This field is required</span>');
        return false;
        }
        if (stddate.length < 1) {
        $('#reservationdate').after('<span class="error">This field is required</span>');
        return false;
        }
        if (reason.length < 1) {
        $('#reason').after('<span class="error">This field is required</span>');
        return false;
        }
        
      
      $('#trnsForm').submit();
      $('#trnsForm')[0].reset();
  })

   $(document).on('click','.feedownbtn',function(){
      
      var feeid=$('#feeid').html();
      var feemonth=$(this).parent().siblings('.feemonth').find('.feemonthname').text();

      $("#form_id").val(feeid);
      $("#form_month").val(feemonth);
      
      $("#pdf_form").submit();
      
   });
    
   
</script>
</body>
</html>
