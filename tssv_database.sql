-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 29, 2021 at 05:13 AM
-- Server version: 5.7.33
-- PHP Version: 7.3.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tssv_database`
--

-- --------------------------------------------------------

--
-- Table structure for table `account`
--

CREATE TABLE `account` (
  `id` int(11) NOT NULL,
  `p_name` varchar(255) NOT NULL,
  `service` varchar(255) NOT NULL,
  `amount` int(11) NOT NULL,
  `purpose` varchar(255) NOT NULL,
  `paydate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `account`
--

INSERT INTO `account` (`id`, `p_name`, `service`, `amount`, `purpose`, `paydate`, `status`) VALUES
(2, 'test( NURSERY-2,,)', 'Credit', 3210, 'Fees For The Month:Jan To Jul 2021', '2021-03-24 07:36:42', 1),
(3, 'test( NURSERY-2,,)', 'Credit', 3940, 'Fees For The Month:February 2021', '2021-03-24 08:04:33', 1);

-- --------------------------------------------------------

--
-- Table structure for table `addclass`
--

CREATE TABLE `addclass` (
  `id` int(11) NOT NULL,
  `cname` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `addclass`
--

INSERT INTO `addclass` (`id`, `cname`) VALUES
(1, ' NURSERY-1'),
(2, ' NURSERY-2'),
(3, 'STANDARD-1'),
(4, 'STANDARD-2'),
(6, 'STANDARD-3'),
(7, 'STANDARD-4'),
(8, 'STANDARD-5'),
(9, 'STANDARD-6');

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `name`, `email`, `password`) VALUES
(1, 'admin', 'admin@gmail.com', 'a2e48ae5d8524e83c78d2abf991df64981dc9bdb52d04dc20036dbd8313ed055');

-- --------------------------------------------------------

--
-- Table structure for table `fees_table`
--

CREATE TABLE `fees_table` (
  `id` int(11) NOT NULL,
  `service` varchar(255) NOT NULL DEFAULT 'Credit',
  `sid` int(11) NOT NULL,
  `amount` int(11) DEFAULT NULL,
  `tfee` varchar(255) NOT NULL,
  `dfee` varchar(255) NOT NULL DEFAULT '0',
  `addmission` varchar(255) NOT NULL DEFAULT '0',
  `game` varchar(255) NOT NULL DEFAULT '0',
  `sfee` varchar(255) NOT NULL DEFAULT '0',
  `mfee` varchar(255) NOT NULL DEFAULT '0',
  `ele` varchar(255) NOT NULL DEFAULT '0',
  `est` varchar(255) NOT NULL DEFAULT '0',
  `cfee` varchar(255) NOT NULL,
  `efee` varchar(255) NOT NULL DEFAULT '0',
  `tour` varchar(255) NOT NULL DEFAULT '0',
  `vfee` varchar(255) NOT NULL DEFAULT '0',
  `other` varchar(255) NOT NULL DEFAULT '0',
  `total` varchar(255) NOT NULL,
  `feetype` varchar(255) NOT NULL,
  `month` varchar(255) DEFAULT NULL,
  `feedate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fees_table`
--

INSERT INTO `fees_table` (`id`, `service`, `sid`, `amount`, `tfee`, `dfee`, `addmission`, `game`, `sfee`, `mfee`, `ele`, `est`, `cfee`, `efee`, `tour`, `vfee`, `other`, `total`, `feetype`, `month`, `feedate`, `status`) VALUES
(2, 'Credit', 5, NULL, '2000', '500', '410', '50', '50', '50', '50', '', '100', '50', '', '', '', '3210', 'multi', 'Jan To Jul 2021', '2021-03-24 00:36:42', 1),
(3, 'Credit', 5, NULL, '3530', '', '410', '', '', '', '', '', '', '', '', '', '', '3940', 'single', 'February 2021', '2021-03-24 01:04:33', 1);

-- --------------------------------------------------------

--
-- Table structure for table `income`
--

CREATE TABLE `income` (
  `id` int(11) NOT NULL,
  `service` varchar(255) NOT NULL,
  `payment` varchar(255) NOT NULL,
  `amount` varchar(255) NOT NULL,
  `purpose` varchar(255) NOT NULL,
  `paydate` datetime NOT NULL,
  `status` int(1) NOT NULL,
  `type` varchar(255) NOT NULL DEFAULT 'inc',
  `transaction` varchar(1) NOT NULL DEFAULT 't'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `studentinfo`
--

CREATE TABLE `studentinfo` (
  `id` int(11) NOT NULL,
  `sname` varchar(255) NOT NULL,
  `gname` varchar(255) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `address` text NOT NULL,
  `postoffice` varchar(255) NOT NULL,
  `village` varchar(255) NOT NULL,
  `policesta` varchar(255) NOT NULL,
  `dist` varchar(255) NOT NULL,
  `pincode` varchar(255) NOT NULL,
  `aadhar` varchar(255) DEFAULT NULL,
  `gender` varchar(255) NOT NULL,
  `cdate` varchar(255) NOT NULL,
  `phone1` varchar(255) NOT NULL,
  `phone2` varchar(255) DEFAULT NULL,
  `class` varchar(255) NOT NULL,
  `section` varchar(255) DEFAULT NULL,
  `roll` varchar(255) DEFAULT NULL,
  `blood` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `studentinfo`
--

INSERT INTO `studentinfo` (`id`, `sname`, `gname`, `image`, `email`, `address`, `postoffice`, `village`, `policesta`, `dist`, `pincode`, `aadhar`, `gender`, `cdate`, `phone1`, `phone2`, `class`, `section`, `roll`, `blood`) VALUES
(5, 'test', 'Test', '', '', 'ghdhhfdhhfd', 'dfdfd', 'dfdfd', 'dfdfd', 'dfdfdf', 'fdfdfd', '', 'Female', '17/03/2021', '1234567890', '', '2', '', '', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `account`
--
ALTER TABLE `account`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `addclass`
--
ALTER TABLE `addclass`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fees_table`
--
ALTER TABLE `fees_table`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `income`
--
ALTER TABLE `income`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `studentinfo`
--
ALTER TABLE `studentinfo`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `account`
--
ALTER TABLE `account`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `addclass`
--
ALTER TABLE `addclass`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `fees_table`
--
ALTER TABLE `fees_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `income`
--
ALTER TABLE `income`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `studentinfo`
--
ALTER TABLE `studentinfo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
